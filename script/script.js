//EDQ FRONTEND SCRIPT INIT
$(document).ready(function() {
    
    $(".sectionTabs").each(function() {                            
        $(this).click(function() {
            $(".sectionTabs").removeClass("activeChanger");
            $(this).addClass("activeChanger");
            $(".scopeSection").hide();
            var thisEQ = $(this).index();
            $('.scopeSection').eq(thisEQ).show();

        });                            
    });
    $(".sectionTabs2").each(function() {                            
        $(this).click(function() {
            $(".sectionTabs2").removeClass("activeChanger");
            $(this).addClass("activeChanger");
            $(".groupSection").hide();
            var thisEQ = $(this).index();
            $('.groupSection').eq(thisEQ).show();

        });                            
    });
    //
    /////////////////////////////
    //On load Scripts//
    /////////////////////////////
    $("#my-menu").mmenu({"extensions": ["fx-listitems-slide","border-offset","pagedim-black","shadow-page","theme-dark"]}, {offCanvas: {pageSelector: "#main-wrap"}});
    
    //Modal Settings
    $("#modal").iziModal({title:"",subtitle:"",headerColor:"#88A0B9",theme:"",appendTo:".body",icon:null,iconText:null,iconColor:"",rtl:!1,width:600,top:null,bottom:null,borderBottom:!0,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:!1,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(){},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    $("#modalAdd").iziModal({title:"ADD ",subtitle:"",headerColor:"#d3d754",theme:"light",appendTo:".body",icon:null,iconText:null,iconColor:"",rtl:!1,width:600,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:!1,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(){},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    $("#modalAddGroup").iziModal({title:"ADD ",subtitle:"",headerColor:"#d3d754",theme:"light",appendTo:".body",icon:null,iconText:null,iconColor:"",rtl:!1,width:600,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:!1,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(modal){modal.startLoading();},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    $("#modalViewScopeRuns").iziModal({title:"View Rundetails",subtitle:"",headerColor:"#d3d754",theme:"light",appendTo:".body",icon:null,iconText:null,iconColor:"",rtl:!1,width:600,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:!1,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(modal){modal.startLoading();},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    							
    
    $("#modalUpdate").iziModal({title:"Are you sure you want to UPDATE the below:",subtitle:"",headerColor:"#d3d754",theme:"light",appendTo:".body",icon:null,iconText:null,iconColor:"",rtl:!1,width:400,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:!1,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){}, onOpening: function(){},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    
    $("#modalDelete").iziModal({title:"Are you sure you want to DELETE the below:",subtitle:"",headerColor:"#d3d754",theme:"light",appendTo:".body",icon:null,iconText:null,iconColor:"",rtl:!1,width:400,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:!1,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(){},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    $("#modalSuccess").iziModal({title:"Success",subtitle:"",headerColor:"rgb(0, 175, 102)",attached:"bottom",theme:"light",appendTo:".body",icon:".success",iconText:null,iconColor:"#fffff",rtl:!1,width:600,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:2e3,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(){},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    $("#modalError").iziModal({title:"Error",subtitle:"",headerColor:"rgb(189, 91, 91)",attached:"bottom",theme:"light",appendTo:".body",icon:".error",iconText:null,iconColor:"",rtl:!1,width:600,top:null,bottom:null,borderBottom:!1,padding:0,radius:3,zindex:999,iframe:!1,iframeHeight:400,iframeURL:null,focusInput:!0,group:"",loop:!1,navigateCaption:!0,navigateArrows:!0,history:!1,restoreDefaultContent:!1,autoOpen:0,bodyOverflow:!1,fullscreen:!1,openFullscreen:!1,closeOnEscape:!0,closeButton:!0,overlay:!0,overlayClose:!0,overlayColor:"rgba(0, 0, 0, 0.4)",timeout:null,timeoutProgressbar:!1,pauseOnHover:!1,timeoutProgressbarColor:"rgba(255,255,255,0.5)",transitionIn:"comingIn",transitionOut:"comingOut",transitionInOverlay:"fadeIn",transitionOutOverlay:"fadeOut",onFullscreen:function(){},onResize:function(){},onOpening:function(){},onOpened:function(){},onClosing:function(){},onClosed:function(){}});
    
    $(document).on('click', '.addButton', function (event) {
        event.preventDefault();
        var targetID = event.target.id;
        
        if (targetID == "clientAdd") {
            var submitType = "client";
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" data-addtype=\"' + submitType + '\">' + 
                                      '<input class="modalInput" type=\"text\" data-field=\"client_name\" placeholder=\"Client Name\" />' +
                                      '<input class="modalInput" type=\"text\" data-field=\"EAGLE_client_name\" placeholder=\"EAGLE Client Name\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"EAGLE_server\" placeholder=\"EAGLE Server\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"client_display_name\" placeholder=\"Client Display Name\"/>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }else if(targetID == "serverAdd") {
            var submitType = "server";
            
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" data-addtype=\"' + submitType + '\">' + 
                                      '<input class="modalInput" type=\"text\" data-field=\"Linked_Server\" placeholder=\"Linked Server\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"IP_address\" placeholder=\"IP Address\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"server_type\" placeholder=\"Server Type\"/>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');            
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }else if(targetID == "databaseAdd") {
            var submitType = "database";
            
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" data-addtype=\"' + submitType + '\">' +
                                      '<select class="modalInput" id=\"databaseServer\" data-field=\"serverInfoID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' +
                                      '<select class="modalInput" id=\"databaseClient\" data-field=\"clientID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"Project\" placeholder=\"Linked Server\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"Table_Catalog\" placeholder=\"IP Address\"/>' +
                                      '<select class="modalInput" data-field=\"Process_Validation\"><option value=\"1\" selected="selected">Yes</option><option value=\"0\">No</option></select>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');
            getServerAfterInit();
            getClientAfterInit();
            
            
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }else if(targetID == "rundetailAdd") {
            var submitType = "rundetail";
            
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" id=\"addRunGroup\" data-addtype=\"' + submitType + '\">' +
                                      '<select class="modalInput" onchange="getDatabaseAfterInit()" id=\"rundetailClient\" data-field=\"DBinfoID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' +
                                      '<select class="modalInput" onchange="getTableAfterInit()" id=\"rundetailDatabase\" data-field=\"DBinfoID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Database</option></select>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"display_name\" placeholder=\"Display Name\"/>' +
                                      '<select class="modalInput" id=\"rundetailTable\" data-field=\"table_name\"><option selected=\"\" disabled=\"\" value=\"default\">choose one</option></select>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"filter\" placeholder=\"Filter\"/>' +
                                      '<select class="modalInput" data-field=\"aggregation\"><option selected=\"\" disabled=\"\" value=\"default\">choose one</option><option value=\"COUNT\">COUNT</option><option value=\"MAX\">MAX</option><option value=\"MIN\">MIN</option><option value=\"AVG\">AVG</option><option value=\"SUM\">SUM</option><option value=\"Count(*)\">Count(*)</option></select>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');            
            getClientAfterInitRundetail();
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }else if(targetID == "groupAdd") {
            var submitType = "group";
            
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" data-addtype=\"' + submitType + '\">' + 
                                      '<select class="modalInput" id=\"groupClient\" data-field=\"clientID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"group_name\" placeholder=\"Group Name\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"group_desc\" placeholder=\"Group Description\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"group_type\" placeholder=\"Group Type\"/>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');         
            getClientAfterInitGroup();
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }else if(targetID == "scopeAdd") {
            var submitType = "scope";
            
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" data-addtype=\"' + submitType + '\">' + 
                                      '<select class="modalInput" id=\"scopeClient\" data-field=\"clientID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"scope_name\" placeholder=\"Scope Name\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"scope_desc\" placeholder=\"Scope Description\"/>' +
                                      '<select class="modalInput" data-field=\"isActive\"><option value=\"1\" selected="selected">Yes</option><option value=\"0\">No</option></select>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');         
            getClientAfterInitGroup();
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }else if(targetID == "runScopeAdd") {
            var submitType = "runscope";
            
            $("#modalAdd .modalContent").remove();
            
            $("#modalAdd").append('<div class=\"modalContent\" data-addtype=\"' + submitType + '\">' + 
                                      '<select class="modalInput" onchange="getScopeAfterInitGroup()" id=\"runScopeClient\" data-field=\"clientID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' + 
                                      '<select class="modalInput" id=\"runScopeScope\" data-field=\"scopeID\"><option selected=\"\" disabled=\"\" value=\"default\" class="default">Select Client</option></select>' + 
                                      '<select class="modalInput" onchange="getRunInfoAfterInitGroup()" id=\"runScopeRunInfo\" data-field=\"runInfoID\"><option selected=\"\" disabled=\"\" value=\"default\">Select Client</option></select>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"run_name\" placeholder=\"Run Name\"/>' +
                                      '<input class="modalInput" type=\"text\" data-field=\"run_order\" placeholder=\"Run Order\"/>' +
                                      '<a href=\"#\" class=\"myButton\" onclick=\"addFunction(this)\" id=\"addSubmit\"><i class="fa fa-check" aria-hidden="true"></i></a>' +
                                  '</div>');
            getClientAfterInitGroup();
            $("#runScopeScope").prop('disabled', true);
            getRunInfoAfterInitGroup();
            $('#modalAdd').iziModal('open');
            $("#modalAdd .iziModal-header-title span").remove();
            $("#modalAdd .iziModal-header-title").append('<span>' + submitType.toUpperCase() + '</span>');
            
        }
    });
    
    //Navigation
    $(".pageCont").hide();
    $('.pageCont[data-page="client"]').show();
    $("#currentPageInner").text($(".mm-selected").text());
    $(".pageItems").each(function() {
       $(this).click(function() {
           var dataType =  $(this).data("page");
           $(".pageItems").removeClass('Selected mm-selected');
           $(this).addClass('Selected mm-selected');
           $(".pageCont").hide();
           $('.pageCont[data-page=\"' + dataType + '\"]').fadeIn();
           $("#currentPageInner").slideUp();
           setTimeout(function(){
               $("#currentPageInner").slideDown();
               $("#currentPageInner").text($(".mm-selected").text());
            }, 300);
           
       });
    });
    
    //Field Widths
    $('.clientDivTableHeaders, .serverDivTableHeaders, .databaseDivTableHeaders,.rundetailDivTableHeaders,.groupDivTableHeaders,.scopeDivTableHeaders,.runScopeDivTableHeaders,.runInfoDivTableHeaders').each(function(){
        var parentChildren = $(this).parent().children().length;
        $(this).outerWidth((100 / parentChildren) + "%");
        
    });

    //Client View
    $.get("http://172.21.132.79/api/client/", function (data) {
        $.each(data, function(index, value){
            $("#clientValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"client_name\">' + data[index].client_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"EAGLE_client_name\">' + data[index].EAGLE_client_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"EAGLE_server\">' + data[index].EAGLE_server + 
                            '</div><div class=\"fields\" contenteditable data-field=\"client_display_name\">' + data[index].client_display_name + 
                            '</div></div>')
            $("#databaseClient, #rundetailClientSelect, #SpecificRundetailClientSelect").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');
        });
        $('#clientValues .fields').each(function(){
            $(this).outerWidth((100 / 5) + "%");
        });
        var contextSelector = $();
        addContextMenu();
    });
    //Server View
    $.get("http://172.21.132.79/api/server/", function (data) {
        $.each(data, function(index, value){
            $("#serverValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"serverInfoID\">' + data[index].serverInfoID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"Linked_Server\">' + data[index].Linked_Server + 
                            '</div><div class=\"fields\" contenteditable data-field=\"IP_address\">' + data[index].IP_address + 
                            '</div><div class=\"fields\" contenteditable data-field=\"server_type\">' + data[index].server_type + 
                            '</div></div>')
        });
//        $("#rundetailClientSelect").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');

        $('#serverValues .fields').each(function(){
            $(this).outerWidth((100 / 4) + "%");
        });
        addContextMenu();
    });
    //Database View
    $.get("http://172.21.132.79/api/database/", function (data) {
        $.each(data, function(index, value){
            $("#databaseValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"DBinfoID\">' + data[index].DBinfoID + 
                            '</div><div class=\"fields int\" data-field=\"serverInfoID\">' + data[index].serverInfoID + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"Project\">' + data[index].Project + 
                            '</div><div class=\"fields\" contenteditable data-field=\"Table_Catalog\">' + data[index].Table_Catalog + 
                            '</div><div class=\"fields\" contenteditable data-field=\"Process_Validation\">' + data[index].Process_Validation + 
                            '</div></div>')
        });
        $('#databaseValues .fields').each(function(){
            $(this).outerWidth((100 / 6) + "%");
        });
        addContextMenu();
    });
    $.get("http://172.21.132.79/api/group/", function (data) {
        $.each(data, function(index, value){
            $("#groupValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"groupID\">' + data[index].groupID + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"group_name\">' + data[index].group_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"group_desc\">' + data[index].group_desc + 
                            '</div><div class=\"fields\" contenteditable data-field=\"group_type\">' + data[index].group_type +
                            '</div></div>')
        });
        $('#groupValues .fields').each(function(){
            $(this).outerWidth((100 / 5) + "%");
        });
        addContextMenu();
    });
    $.get("http://172.21.132.79/api/scope/", function (data) {
        $.each(data, function(index, value){
            $("#scopeValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"scopeID\">' + data[index].scopeID + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"scope_name\">' + data[index].scope_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"scope_desc\">' + data[index].scope_desc + 
                            '</div><div class=\"fields\" contenteditable data-field=\"isActive\">' + data[index].isActive +
                            '</div></div>')
        });
        $('#scopeValues .fields').each(function(){
            $(this).outerWidth((100 / 5) + "%");
        });
        addScopeContextMenu();
    }); 
    $.get("http://172.21.132.79/api/runscope/", function (data) {
        $.each(data, function(index, value){
            $("#runScopeValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"runScopeID\">' + data[index].runScopeID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"run_name\">' + data[index].run_name + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields int\" data-field=\"scopeID\">' + data[index].scopeID + 
                            '</div><div class=\"fields int\" contenteditable data-field=\"run_order\">' + data[index].run_order + 
                            '</div><div class=\"fields int\" data-field=\"runInfoID\">' + data[index].runInfoID +
                            '</div></div>');
        });
        $('#runScopeValues .fields').each(function(){
            $(this).outerWidth((100 / 6) + "%");
        });
        addContextMenu();
    }); 
    $.get("http://172.21.132.79/api/runinfo/", function (data) {
        $.each(data, function(index, value){
            $("#runInfoValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"runInfoID\">' + data[index].runInfoID + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"run_name\">' + data[index].run_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"run_display_name\">' + data[index].run_display_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"run_type\">' + data[index].run_type +
                            '</div></div>')
        });
        $('#runInfoValues .fields').each(function(){
            $(this).outerWidth((100 / 5) + "%");
        });
        addContextMenu();
    });
    $('[class*=DivTableHeaders]').each(function() {
            $(this).on('click', function () {
                var currentIndex = $(this).index();
                var thisContext = $(this);
                var $divsThis = $(this).closest(".norm").find(".fieldRow");
                var $fieldThis = $divsThis.find('.fields:eq(' + currentIndex + ')');
                var $parentThis = $(this).closest(".norm").find('[id*=Values]');
                
                if ($(this).hasClass("currentSorted")) {
                    console.log("has Class")
                    if($(this).data("upDown") == "down") {
                        console.log("is down");
                        sortReversedUsingNestedText($parentThis, $divsThis);
                    }else {
                        console.log("is up")
                        sortUsingNestedText($parentThis, $divsThis);
                    }
                    
                }else {
                    console.log("No Class");
                    thisContext.siblings().removeClass("currentSorted");
                    $(this).addClass("currentSorted");
                    $(this).removeAttr('data-upDown');
                    $(this).attr('data-upDown');
                    sortUsingNestedText($parentThis, $divsThis);
                }
                
                function sortUsingNestedText(parent, $divs) {
                    $(".currentSorted").data("upDown", "down");
                    var items = parent.children($divs).sort(function(a, b) {
                        if($fieldThis.hasClass("int")) {
                            var vAtemp = $(a).find('.fields:eq(' + currentIndex + ')').text();
                            var vBtemp = $(b).find('.fields:eq(' + currentIndex + ')').text();
                            var vA = parseInt(vAtemp);
                            var vB = parseInt(vBtemp);
                            
                        }else {
                            var vA = $(a).find('.fields:eq(' + currentIndex + ')').text().toLowerCase();
                            var vB = $(b).find('.fields:eq(' + currentIndex + ')').text().toLowerCase();
                        }
                        return (vA < vB) ? -1 : (vA > vB) ? 1 : 0;
                        
                    });
                    parent.append(items);
                }
                function sortReversedUsingNestedText(parent, $divs) {
                    $(".currentSorted").data("upDown", "up");
                    var items = parent.children($divs).sort(function(a, b) {
                        if($fieldThis.hasClass("int")) {
                            var vAtemp = $(a).find('.fields:eq(' + currentIndex + ')').text();
                            var vBtemp = $(b).find('.fields:eq(' + currentIndex + ')').text();
                            var vA = parseInt(vAtemp);
                            var vB = parseInt(vBtemp);
                            
                        }else {
                            var vA = $(a).find('.fields:eq(' + currentIndex + ')').text().toLowerCase();
                            var vB = $(b).find('.fields:eq(' + currentIndex + ')').text().toLowerCase();
                           
                        }
                        return (vA > vB) ? -1 : (vA < vB) ? 1 : 0;
                        
                    });
                    parent.append(items);
                }
            });
        });
    
    //EDQ FRONTEND SCRIPT END
});
/////////////////////////////
//General AfterLoad Scripts//
/////////////////////////////

function addContextMenu() {
    $.contextMenu({
        selector: ".norm .fields", 
        callback: function(key, options) {

            if (key == "update") {
                //alert(options.$trigger.parent().find(".fields").text()); 
                updateItem(options.$trigger);
            }else if (key == "delete") {
                //alert(options.$trigger.parent().find(".fields").text()); 
                deleteItem(options.$trigger);
            }

        },
        items: {
            "update": {name: "Update", icon: "edit"},
            "sep1": "---------",
            "delete": {name: "Delete", icon: "edit"}
        },
        events: {
            show: function(opt) {
                    opt.$trigger.parent().addClass("selectedRow");  

            }, 
            hide: function(opt) {
                    opt.$trigger.parent().removeClass("selectedRow");
            }

        }
    });
}
function addRunScopeContextMenu() {
    $.contextMenu({
        selector: "#rundetailPickerCont .runScopeFields", 
        callback: function(key, options) {
            if (key == "delete") {
                deleteRunScopeItem(options.$trigger);
            }
        },
        items: {
            "delete": {name: "Delete", icon: "delete"}
        },
        events: {
            show: function(opt) {
                    opt.$trigger.parent().addClass("selectedRow");  

            }, 
            hide: function(opt) {
                    opt.$trigger.parent().removeClass("selectedRow");
            }

        }
    });
}
function deleteRunScopeItem(triggerField) {
    var thisScopeID = triggerField.parent().find('.runScopeFields:eq(0)').text();
    var deleteURL = "http://172.21.132.79/api/runscope/" + thisScopeID;
    
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: deleteURL,
        type: 'delete',
        dataType: "json",
        contentType: "application/json",
        error: function (e) {
            console.log(e);
            $("#modalError .iziModal-header-title").text("ERROR! Could not Delete." );
            $('#modalError').iziModal('open');
        },
        beforeSend: function () {},
        complete: function () {},
        success: function (result) {
            triggerField.parent().slideUp();
            $('#modalDelete').iziModal('close');
            $("#modalSuccess .iziModal-header-title").text("Success! Item has been Deleted." );
            $('#modalSuccess').iziModal('open');
        }
    });
    
}
function addScopeContextMenu() {
    $.contextMenu({
        selector: "#scopeTable .fields", 
        callback: function(key, options) {

            if (key == "update") {
                //alert(options.$trigger.parent().find(".fields").text()); 
                updateItem(options.$trigger);
            }else if (key == "viewrundetails") {
//                alert(options.$trigger.parent().find(".fields").text()); 
                viewrundetails(options.$trigger);
            }else if (key == "delete") {
                //alert(options.$trigger.parent().find(".fields").text()); 
                deleteItem(options.$trigger);
            }

        },
        items: {
            "update": {name: "Update", icon: "edit"},
            "viewrundetails": {name: "View Run Details", icon: "edit"},
            "sep1": "---------",
            "delete": {name: "Delete", icon: "edit"}
        },
        events: {
            show: function(opt) {
                    opt.$trigger.parent().addClass("selectedRow");  

            }, 
            hide: function(opt) {
                    opt.$trigger.parent().removeClass("selectedRow");
            }

        }
    });
}
function viewrundetails (triggerField) {
    
    var thisScopeID = triggerField.parent().find('.fields:eq(0)').text();
    
    $.get("http://172.21.132.79/api/runscope/", function (data) {
        $("#rundetailPickerCont").find(".runscopeitems").remove();
       
    }).success(function(data) {
        
        $.each(data, function(index, value){
            if (data[index].scopeID == thisScopeID) {
                console.log(data[index].scopeID);
                $("#rundetailPickerCont").append('<div class=\"row runscopeitems\">' +
                    '<div class=\"col-xs-4 rundetailitemtext runScopeFields\">' + data[index].runScopeID + '</div>' +
                    '<div class=\"col-xs-4 rundetailitemtext runScopeFields\">' + data[index].run_name + '</div>' +
                    '<div class=\"col-xs-4 rundetailitemtext runScopeFields\">' + data[index].scopeID + '</div>' +
                '</div>');
            }
        });
    }).error(function(e) {
       
    }).done(function() {
       $(".runscopeitems").each(function() {
          $(this).click(function() {
              $(this).toggleClass("shiftLeft");
          });
       });
    });
    addRunScopeContextMenu();
    $('#modalViewScopeRuns').delay(1000).iziModal('open');
    $("#modalViewScopeRuns").iziModal('stopLoading');
}

function getClientAfterInit() {
     $.get("http://172.21.132.79/api/client/", function (data) {
        $.each(data, function(index, value){
            $("#databaseClient").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');
        });
    });
}
function getClientAfterInitRundetail() {
     $.get("http://172.21.132.79/api/client/", function (data) {
        $.each(data, function(index, value){
            $("#rundetailClient").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');
        });
    });
}
function getClientAfterInitGroup() {
     $.get("http://172.21.132.79/api/client/", function (data) {
        $.each(data, function(index, value){
            $("#groupClient, #scopeClient, #runScopeClient").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');
        });
    });
}
function getScopeAfterInitGroup() {
    var clientId = $("#runScopeClient").val();
    $("#runScopeScope").prop('disabled', false);
    $("#runScopeScope option").not(".default").remove();
    
    $.get("http://172.21.132.79/api/client/" + clientId + "/scope/", function (data) {
        $.each(data, function(index, value){
            $("#runScopeScope").append('<option value=\"' + data[index].scopeID + '\">' + data[index].scope_name + '</option>');
        });
    });
}
function getRunInfoAfterInitGroup() {
     $.get("http://172.21.132.79/api/runInfo/", function (data) {
        $.each(data, function(index, value){
            $("#runScopeRunInfo").append('<option value=\"' + data[index].runInfoID + '\">' + data[index].run_name + '</option>');
        });
    });
}

function getServerAfterInit() {
     $.get("http://172.21.132.79/api/server/", function (data) {
        $.each(data, function(index, value){
            $("#databaseServer").append('<option value=\"' + data[index].serverInfoID + '\">' + data[index].Linked_Server + '</option>');
            

        });
    });
}
function getDatabaseAfterInit() {
    var clientID = $("#rundetailClient").val();
    $('#rundetailDatabase option').remove();
     $.get("http://172.21.132.79/api/client/" + clientID + "/database/", function (data) {
        $.each(data, function(index, value){
            $("#rundetailDatabase").append('<option value=\"' + data[index].DBinfoID + '\">' + data[index].Table_Catalog + '</option>');
        });
    });
}
function getTableAfterInit() {
    var database = $("#rundetailDatabase").val();
    $('#rundetailTable option').remove();
     $.get("http://172.21.132.79/api/database/" + database + "/table/", function (data) {
        $.each(data, function(index, value){
            $("#rundetailTable").append('<option value=\"' + data[index].TableName + '\">' + data[index].TableName + '</option>');
        });
    });
}

function refreshList(e) {
    var targetID = e.id;
    var tableType = $("#" + targetID).closest(".pageCont").data('page');
    $("#" + targetID).closest(".pageCont").find(".refreshThis").fadeOut(200,function(){            
        $(this).find(".fieldRow").each(function(){
            $(this).remove();
        });
        getTables(tableType, targetID);
//        console.log($(this).find(".fieldRow").text());
    });
    
}
function getTables(tableType, targetID) {
    var targetCurrent = $("#" + targetID);
    if (tableType == "client") {
        $.get("http://172.21.132.79/api/client/", function (data) {
            $.each(data, function(index, value){
                $("#clientValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                                '</div><div class=\"fields\" contenteditable data-field=\"client_name\">' + data[index].client_name + 
                                '</div><div class=\"fields\" contenteditable data-field=\"EAGLE_client_name\">' + data[index].EAGLE_client_name + 
                                '</div><div class=\"fields\" contenteditable data-field=\"EAGLE_server\">' + data[index].EAGLE_server + 
                                '</div><div class=\"fields\" contenteditable data-field=\"client_display_name\">' + data[index].client_display_name + 
                                '</div></div>')
                $("#databaseClient").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');
            });
            $('#clientValues .fields').each(function(){
                $(this).outerWidth((100 / 5) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            var contextSelector = $();
            addContextMenu();
        });

    }else if (tableType == "server") {
        $.get("http://172.21.132.79/api/server/", function (data) {
            $.each(data, function(index, value){
                $("#serverValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"serverInfoID\">' + data[index].serverInfoID + 
                                '</div><div class=\"fields\" contenteditable data-field=\"Linked_Server\">' + data[index].Linked_Server + 
                                '</div><div class=\"fields\" contenteditable data-field=\"IP_address\">' + data[index].IP_address + 
                                '</div><div class=\"fields\" contenteditable data-field=\"server_type\">' + data[index].server_type + 
                                '</div></div>')
            });
            $('#serverValues .fields').each(function(){
                $(this).outerWidth((100 / 4) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            addContextMenu();
        });
        
        
    }else if (tableType == "database") {
        $.get("http://172.21.132.79/api/database/", function (data) {
            
            $.each(data, function(index, value){
                $("#databaseValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"DBinfoID\">' + data[index].DBinfoID + 
                                '</div><div class=\"fields int\" data-field=\"serverInfoID\">' + data[index].serverInfoID + 
                                '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                                '</div><div class=\"fields\" contenteditable data-field=\"Project\">' + data[index].Project + 
                                '</div><div class=\"fields\" contenteditable data-field=\"Table_Catalog\">' + data[index].Table_Catalog + 
                                '</div><div class=\"fields\" contenteditable data-field=\"Process_Validation\">' + data[index].Process_Validation + 
                                '</div></div>')
            });
            $('#databaseValues .fields').each(function(){
                $(this).outerWidth((100 / 6) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            addContextMenu();
        });
        
    }else if (tableType == "group") {
        $.get("http://172.21.132.79/api/group/", function (data) {
            
            $.each(data, function(index, value){
                 $("#groupValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"groupID\">' + data[index].groupID + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"group_name\">' + data[index].group_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"group_desc\">' + data[index].group_desc + 
                            '</div><div class=\"fields\" contenteditable data-field=\"group_type\">' + data[index].group_type +
                            '</div></div>')
            });
            $('#groupValues .fields').each(function(){
                $(this).outerWidth((100 / 5) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            addContextMenu();
        });
        
    }else if (tableType == "scope") {
        $.get("http://172.21.132.79/api/scope/", function (data) {
            
            $.each(data, function(index, value){
            $("#scopeValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"scopeID\">' + data[index].scopeID + 
                            '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                            '</div><div class=\"fields\" contenteditable data-field=\"scope_name\">' + data[index].scope_name + 
                            '</div><div class=\"fields\" contenteditable data-field=\"scope_desc\">' + data[index].scope_desc + 
                            '</div><div class=\"fields\" contenteditable data-field=\"isActive\">' + data[index].isActive +
                            '</div></div>')
            });
            $('#scopeValues .fields').each(function(){
                $(this).outerWidth((100 / 5) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            console.log("haha");
            addScopeContextMenu();
        });
        
    }else if (tableType == "runscope") {
        $.get("http://172.21.132.79/api/runscope/", function (data) {
            
            $.each(data, function(index, value){
                $("#runScopeValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"runScopeID\">' + data[index].runScopeID + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"run_name\">' + data[index].run_name + 
                                    '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                                    '</div><div class=\"fields int\" data-field=\"scopeID\">' + data[index].scopeID + 
                                    '</div><div class=\"fields int\" contenteditable data-field=\"run_order\">' + data[index].run_order + 
                                    '</div><div class=\"fields int\" data-field=\"runInfoID\">' + data[index].runInfoID +
                                    '</div></div>')
            });
            $('#runScopeValues .fields').each(function(){
                $(this).outerWidth((100 / 6) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            addContextMenu();
        });
        
    }else if (tableType == "runinfo") {
        $.get("http://172.21.132.79/api/runinfo/", function (data) {
            
            $.each(data, function(index, value){
                $("#runInfoValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields int\" data-field=\"runInfoID\">' + data[index].runInfoID + 
                                '</div><div class=\"fields int\" data-field=\"clientID\">' + data[index].clientID + 
                                '</div><div class=\"fields\" contenteditable data-field=\"run_name\">' + data[index].run_name + 
                                '</div><div class=\"fields\" contenteditable data-field=\"run_display_name\">' + data[index].run_display_name + 
                                '</div><div class=\"fields\" contenteditable data-field=\"run_type\">' + data[index].run_type +
                                '</div></div>')
            });
            $('#runInfoValues .fields').each(function(){
                $(this).outerWidth((100 / 5) + "%");
            });
            targetCurrent.closest(".pageCont").find(".refreshThis").fadeIn();
            addContextMenu();
        });
        
    }
}
 
function addFunction (e) {
    var thisItem = e.id;
    var submitType = $("#" + thisItem).parent().data("addtype");
    var submitArray = {};
    var countFields = 0;
    var countValues = 0;
    
    var  inputs = $("#" + thisItem).parent().find('.modalInput');
    $(inputs).each(function() {
        countFields++;
        console.log($(this).val());
        if ($(this).val().length >= 1) {
            countValues++;
        }
    });
    
    if (countFields == countValues){
        
        $("#" + thisItem).parent().find('input, select').each(function() {
        
            submitArray[$(this).data("field")] = $(this).val();
        
        });
   
        var jsonSubmitArray = JSON.stringify(submitArray);
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: 'http://172.21.132.79/api/' + submitType,
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data:jsonSubmitArray,
            error: function (e) {
                console.log(e);
                $("#modalError .iziModal-header-title").text("ERROR! " + submitType + " was not added, try again." );
                $('#modalError').iziModal('open');
            },
            beforeSend: function () {
               

            },
            complete: function () {
                
            },
            success: function (result) {
                $("#modalAdd").iziModal('close');
                $("#modalSuccess .iziModal-header-title").text("Success! " + submitType + " has been added." );
                $('#modalSuccess').iziModal('open');
            }
        });
        
        
    }else {
        $("#modalError .iziModal-header-title").text("Please complete all fields.")
        $('#modalError').iziModal('open');
    }
}

function updateItem(triggerField){
    $("#modalUpdate #updateFields .updateVals").remove();
    $(".currentTrigger").removeClass('currentTrigger');
    triggerField.addClass('currentTrigger');
    
    var updateArray = {};
    
    triggerField.parent().find('.fields').each(function() {
        updateArray[$(this).data("field")] = $(this).text();
        
    });
     $.each(updateArray, function(k, v) {
        $("#modalUpdate #updateFields").append('<div class=\"updateVals\">' + '<span class=\"boldText\">' + k + ': </span>' + v + '</div>');
    });
    
    $('#modalUpdate').delay(1000).iziModal('open');
    
   

}

function modalUpdateYes() {
    
    var updateArray = {};
    var arrayJustVals = [];
    var triggerField = $(".currentTrigger");
    
    triggerField.parent().find('.fields').each(function() {
        arrayJustVals.push($(this).text()); 
        updateArray[$(this).data("field")] = $(this).text();
        
    });
    
    var urlType = triggerField.parent().parent().parent().parent().data('page');
    var urlID = arrayJustVals[0];
    var updateURL = "http://172.21.132.79/api/" + urlType + "/" + urlID;
    var jsonUpdateArray = JSON.stringify(updateArray);
    
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: updateURL,
        type: 'PUT',
        dataType: "json",
        contentType: "application/json",
        data: jsonUpdateArray,
        error: function (e) {
            console.log(e);
            $("#modalError .iziModal-header-title").text("ERROR! Could not update." );
            $('#modalError').iziModal('open');
        },
        beforeSend: function () {},
        complete: function () {},
        success: function (result) {
            console.log(updateURL)
            console.log("--------------")
            console.log(jsonUpdateArray)
            $('#modalUpdate').iziModal('close');
            $("#modalSuccess .iziModal-header-title").text("Success! Item has been updated." );
            $('#modalSuccess').iziModal('open');
        }
    });
    
    
    
    
}

function deleteItem(triggerField){
    
    $("#modalDelete #updateFields .updateVals").remove();
    $(".currentTrigger").removeClass('currentTrigger');
    triggerField.addClass('currentTrigger');
    
    var updateArray = {};
    
    triggerField.parent().find('.fields').each(function() {
        updateArray[$(this).data("field")] = $(this).text();
        
    });
    $.each(updateArray, function(k, v) {
        $("#modalDelete #updateFields").append('<div class=\"updateVals\">' + '<span class=\"boldText\">' + k + ': </span>' + v + '</div>');
    });
    
    $('#modalDelete').delay(1000).iziModal('open');
    
   

}

function modalDeleteYes() {
    
    var deleteArray = {};
    var arrayJustVals = [];
    var triggerField = $(".currentTrigger");
    
    triggerField.parent().find('.fields').each(function() {
        arrayJustVals.push($(this).text()); 
        deleteArray[$(this).data("field")] = $(this).text();
        
    });
    
    var urlType = triggerField.parent().parent().parent().parent().data('page');
    var urlID = arrayJustVals[0];
    var deleteURL = "http://172.21.132.79/api/" + urlType + "/" + urlID;
    var jsonDeleteArray = JSON.stringify(deleteArray);
    
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: deleteURL,
        type: 'delete',
        dataType: "json",
        contentType: "application/json",
        error: function (e) {
            console.log(e);
            $("#modalError .iziModal-header-title").text("ERROR! Could not Delete." );
            $('#modalError').iziModal('open');
        },
        beforeSend: function () {},
        complete: function () {},
        success: function (result) {
            console.log(deleteURL)
            console.log("--------------")
            console.log(jsonDeleteArray)
            $('#modalDelete').iziModal('close');
            $("#modalSuccess .iziModal-header-title").text("Success! Item has been Deleted." );
            $('#modalSuccess').iziModal('open');
        }
    });
    
}

$(document).ready(function() {
    $("#selectContainerCross").click(function() {
        $("#optionSelect option").not(".default").remove();
        $("#optionSelect").val("default");
        $("#clientServerSelect").val("default");
        $("#selectContainerCross").removeClass("crossWidth");
        $("#searchButton").removeClass("searchWidth");
        $("#databaseValues .fieldRow").show();
    });

    $("#searchButton").click(function() {
        if ($("#optionSelect").val() !== "default" && $("#clientServerSelect").val() !== "default" ) {
            $("#selectContainerCross").addClass("crossWidth");
            $("#searchButton").addClass("searchWidth");
        }else {
        }

        if ($("#clientServerSelect").val() == "client") {
            var searchVal =  $("#optionSelect").val();
            $("#databaseValues .fieldRow").each(function() {
                var col = $(this).find(".fields:eq(2)");
                if (col.text() !== searchVal) {
                    col.parent().hide();
                }else {
                    col.parent().show();
                }
            });

        } else if ($("#clientServerSelect").val() == "server") {
            var searchVal =  $("#optionSelect").val();
            $("#databaseValues .fieldRow").each(function() {
                var col = $(this).find(".fields:eq(1)");
                if (col.text() !== searchVal) {
                    col.parent().hide();
                }else {
                    col.parent().show();
                }
            });

        } else {


        }
    });
    $("#clientServerSelect").on("change", function() {
        $("#optionSelect option").not(".default").remove();
        if ($("#clientServerSelect").val() == "client") {
            $.get("http://172.21.132.79/api/client/", function (data) {
                $.each(data, function(index, value){
                    $("#optionSelect").append('<option value=\"' + data[index].clientID + '\">' + data[index].client_name + '</option>');
                });
            });
        }else if ($("#clientServerSelect").val() == "server"){
            $.get("http://172.21.132.79/api/server/", function (data) {
                $.each(data, function(index, value){
                    $("#optionSelect").append('<option value=\"' + data[index].serverInfoID + '\">' + data[index].Linked_Server + '</option>');
                });
            });
        } else {
             $("#optionSelect option").not(".default").remove();
        }
    });
     $(".searchBar").each(function () {
        $(this).keyup(function () {
            _this = this;
            _thisBridge = $(this).closest(".pageCont").find('.fieldRow');
            $.each(_thisBridge, function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1){
                    $(this).slideUp('fast');
                }else {
                    $(this).slideDown('fast');
                }
            });
            
            
        });
    }); 
    $(".searchBarGroup").each(function () {
        $(this).keyup(function () {
            _this = this;
            _thisBridge = $(this).closest(".searchGroupCont").find('.fieldRow');
            $.each(_thisBridge, function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
                    $(this).slideUp('fast');
                }else {
                    $(this).slideDown('fast');
                }
                    
            });
            
        });
    });
});
$(document).ready(function() {
    var options = {
        url: "http://172.21.132.79/api/rundetail",
        getValue: "table_name",
        getValue: function(element) {
            return element.table_name;
        },
        list: {
            onChooseEvent: function() {
                var value = $("#searchAuto").getSelectedItemData().id;
            },
            match: {
                enabled: true
            }
        }
    };

    $("#searchAuto").easyAutocomplete(options);
    $("#clientDone, #clientFail, #databaseDone, #databaseFail").hide();
                               
    $(".hideLinks").hide();
    $("#rundetailTable, .pager").hide();
    $("#SpecificRundetailClientSelect").on("change", function() {
        $("#rundetailTable, .pager").hide();
        $("#SpecificRundetailDatabaseSelect option").not(".default").remove();
        $("#SpecificRundetailValues .fieldRow").remove();
        var selectVal = $("#SpecificRundetailClientSelect").val();
        $.get("http://172.21.132.79/api/client/" + selectVal + "/database", function (data) {
            $.each(data, function(index, value){
                $("#SpecificRundetailDatabaseSelect").append('<option value=\"' + data[index].DBinfoID + '\">' + data[index].Table_Catalog + '</option>');
            });
        });
        if ($("#cmn-toggle-4").is(":checked")) {
            console.log("working");
            $("#SpecificRundetailValues").removeData("url");
            $("#SpecificRundetailValues").removeData("min");
            $("#SpecificRundetailValues").removeData("max");
            $("#SpecificRundetailValues").removeData("add");
            
        }else {
            console.log("not working");
            var thisVal = $("#SpecificRundetailClientSelect").val();
            getSpecificRundetailValuesFromClient(thisVal);
            
        }
        
    });
    $("#SpecificRundetailDatabaseSelect").prop('disabled', true);
    $("#cmn-toggle-4").click(function(){
        $("#rundetailTable, .pager").hide();
        if($(this).is(":checked")) {
           $("#SpecificRundetailDatabaseSelect").prop('disabled', false);
        } else {
           $("#SpecificRundetailDatabaseSelect").prop('disabled', true);
        }

    });
    $("#SpecificRundetailDatabaseSelect").on("change", function() {
        $("#SpecificRundetailValues .fieldRow").remove();
        $(".pager").hide();
        var selectVal = $("#SpecificRundetailDatabaseSelect").val();
        getSpecificRundetailValues(selectVal);
    });
    
    $("#rundetailClientSelect").on("change", function() {
        $(".hideLinks").hide();
        $("#rundetailDatabaseSelect option").not(".default").remove();
        $("#runGroupValues .fieldRow").remove();
        $("#runDetailValues .fieldRow").remove();
        var selectVal = $("#rundetailClientSelect").val();
        $.get("http://172.21.132.79/api/client/" + selectVal + "/database", function (data) {
            
                $.each(data, function(index, value){
                    $("#rundetailDatabaseSelect").append('<option value=\"' + data[index].DBinfoID + '\">' + data[index].Table_Catalog + '</option>');
                });
                getClientGroups(selectVal)
        }); 
    });
    $("#rundetailDatabaseSelect").on("change", function() {
        $("#runDetailValues .fieldRow").remove();
        var selectVal = $("#rundetailDatabaseSelect").val();
        getClientRunDetailsManage(selectVal)
    });

    $("#nextRuns").click(function() {
        var minVal = $("#SpecificRundetailValues").data("min"), 
            maxVal = $("#SpecificRundetailValues").data("max"),
            urlVal = $("#SpecificRundetailValues").data("url"),
            type = "next";
        console.log(minVal + " : " +maxVal);
        getRundetailNextOrPrev(type, urlVal, minVal, maxVal);
        
    }); 
    $("#previousRuns").click(function() {
        var minVal = $("#SpecificRundetailValues").data("min"), 
            maxVal = $("#SpecificRundetailValues").data("max"),
            urlVal = $("#SpecificRundetailValues").data("url"),
            type = "previous";
        console.log(minVal + " : " +maxVal);
        getRundetailNextOrPrev(type, urlVal, minVal, maxVal);
        
    });

});

function getRundetailNextOrPrev(type, url, min, max) {
    var clientList = [];
    var absoluteMax = [];
    $.get(url, function (data) {
        absoluteMax.push(data.length);
        if (data.length > 0) {
            $.each(data, function(k, v) {
                clientList.push(v.ref);
            });        
        } else {
            $("#rundetailTable, .pager").hide();
            $("#modalError .iziModal-header-title").text("Please select another Client" );
            $('#modalError').iziModal('open');
        }
    }).done(function () {
        var maxVal = absoluteMax[0] - 1;
        var minVal = 0;
        var additionRemoval = [];
        
        if (type == "next"){
            if (maxVal >= (max + 20))  {
                min = min + 20;
                max = max + 20;
                var number = +($("#updatePageNumber").text())
                $("#updatePageNumber").text(number + 1);
                getRundetailsFunc(clientList, min, max);
                $("#SpecificRundetailValues").data("min", min);
                $("#SpecificRundetailValues").data("max", max);
            }else if(maxVal < (max + 20) && maxVal > max) {
                min = max + 1;
                max = maxVal;
                var number = +($("#updatePageNumber").text())
                $("#updatePageNumber").text(number + 1);
                getRundetailsFunc(clientList, min, max);
                $("#SpecificRundetailValues").data("add", max - min);
                $("#SpecificRundetailValues").data("min", min);
                $("#SpecificRundetailValues").data("max", max);
            }else if(maxVal == max) {
                console.log("we're maxed bro");
            }
        }else if (type == "previous") {
            if ((min - 20) < 0) {
                max = 19;
                min = 0;
                $("#SpecificRundetailValues").data("min", min);
                $("#SpecificRundetailValues").data("max", max);
                console.log("we're as low as can go bro");
                
            }else if((max-min) < 19){
                var dataAdd = $("#SpecificRundetailValues").data("add");    
                min = min - 20;
                max = max - (dataAdd + 1);
                var number = +($("#updatePageNumber").text())
                $("#updatePageNumber").text(number - 1);
                getRundetailsFunc(clientList, min, max);
                $("#SpecificRundetailValues").data("min", min);
                $("#SpecificRundetailValues").data("max", max);
            }else{
                min = min - 20;
                max = max - 20;
                var number = +($("#updatePageNumber").text())
                $("#updatePageNumber").text(number - 1);
                getRundetailsFunc(clientList, min, max);
                $("#SpecificRundetailValues").data("min", min);
                $("#SpecificRundetailValues").data("max", max);
            }
            
        }  
    });
}

function getRundetailsFunc(clientList, min, max) {
    $("#rundetailTable, .pager").hide();
    $("#rundetailTable").find(".fieldRow").each(function() {
            $(this).remove();
    });
    var count = 0;
    var printArray = [];
    $.each(clientList, function (index) {
        if (index >= min && index <= max ) {
             count++;
            var getData = clientList[index];
            $.get(getData, function (data) {
               
            }).success(function(data) {
                $("#SpecificRundetailValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields\" data-field=\"runDetailID\">' + data.runDetailID + 
                                    '</div><div class=\"fields\" data-field=\"DBinfoID\">' + data.DBinfoID + 
                                    '</div><div class=\"fields\" data-field=\"table_name\" contenteditable> ' + data.table_name + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"field_name\">' + data.field_name + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"aggregation\">' + data.aggregation + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"filter\">' + data.filter + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"isActive\">' + data.isActive + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"table_schema\">' + data.table_schema + 
                                    '</div><div class=\"fields\" contenteditable data-field=\"display_name\">' + data.display_name + 
                                    '</div></div>');
                
                $('#SpecificRundetailValues .fields').each(function(){
                    $(this).outerWidth((100 / 9) + "%");
                });
                $("#rundetailTable, .pager").fadeIn();  
                
            });
        }  
    });
    
    
//    $.each(printArray, function(index, value){
//        console.log(value);
//    });
// 
//    console.log(printArray);
//    $("#SpecificRundetailValues").append(printArray.join(''));
    
}


function getSpecificRundetailValuesFromClient(client) {
    var groupURL = "http://172.21.132.79/api/client/" + client + "/rundetail/";
    var clientList = [];
    $("#rundetailTable, .pager").fadeIn();
    $("#SpecificRundetailValues").attr("data-url", groupURL);
    $("#SpecificRundetailValues").attr("data-min", 0);
    $("#SpecificRundetailValues").attr("data-max", 19);
    $("#SpecificRundetailValues").attr("data-add", 0);
    $("#SpecificRundetailValues").data("url", groupURL);
    $("#SpecificRundetailValues").data("min", 0);
    $("#SpecificRundetailValues").data("max", 19);
    $("#SpecificRundetailValues").data("add", 0);
    
    $.get(groupURL, function (data) {
        if (data.length > 0) {
            $.each(data, function(k, v) {
                clientList.push(v.ref);
            });
            if (data.length > 0) {
                $("#updatePageNumber").text("1");
            }
        } else {
            $("#rundetailTable, .pager").hide();
            $("#modalError .iziModal-header-title").text("Please select another Client" );
            $('#modalError').iziModal('open');
        }
    }).done(function () {
        $.each(clientList, function (index) {
            
            if (index < 20) {
                var getData = clientList[index];
                $.get(getData, function (data) {
                        $("#SpecificRundetailValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields\" data-field=\"runDetailID\">' + data.runDetailID + 
                                            '</div><div class=\"fields\" data-field=\"DBinfoID\">' + data.DBinfoID + 
                                            '</div><div class=\"fields\" data-field=\"table_name\" contenteditable> ' + data.table_name + 
                                            '</div><div class=\"fields\" contenteditable data-field=\"field_name\">' + data.field_name + 
                                            '</div><div class=\"fields\" contenteditable data-field=\"aggregation\">' + data.aggregation + 
                                            '</div><div class=\"fields\" contenteditable data-field=\"filter\">' + data.filter + 
                                            '</div><div class=\"fields\" contenteditable data-field=\"isActive\">' + data.isActive + 
                                            '</div><div class=\"fields\" contenteditable data-field=\"table_schema\">' + data.table_schema + 
                                            '</div><div class=\"fields\" contenteditable data-field=\"display_name\">' + data.display_name + 
                                            '</div></div>');
                        $('#SpecificRundetailValues .fields').each(function(){
                            $(this).outerWidth((100 / 9) + "%");
                        });
                });
            }  
        });
    });
    
}


function getSpecificRundetailValues(database) {
    
    console.log(database);
    var groupURL = "http://172.21.132.79/api/database/" + database + "/rundetail/";
    $.get(groupURL, function (data) {
        if (data.length > 0) {
            $("#rundetailTable").show();
            $.each(data, function(index, value){
               
                $("#SpecificRundetailValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields\" data-field=\"runDetailID\">' + data[index].runDetailID + 
                                '</div><div class=\"fields\" data-field=\"DBinfoID\">' + data[index].DBinfoID + 
                                '</div><div class=\"fields\" data-field=\"table_name\" contenteditable> ' + data[index].table_name + 
                                '</div><div class=\"fields\" contenteditable data-field=\"field_name\">' + data[index].field_name + 
                                '</div><div class=\"fields\" contenteditable data-field=\"aggregation\">' + data[index].aggregation + 
                                '</div><div class=\"fields\" contenteditable data-field=\"filter\">' + data[index].filter + 
                                '</div><div class=\"fields\" contenteditable data-field=\"isActive\">' + data[index].isActive + 
                                '</div><div class=\"fields\" contenteditable data-field=\"table_schema\">' + data[index].table_schema + 
                                '</div><div class=\"fields\" contenteditable data-field=\"display_name\">' + data[index].display_name + 
                                '</div></div>');
            }); 
            $('#SpecificRundetailValues .fields').each(function(){
                $(this).outerWidth((100 / 9) + "%");
            });
        } else {
            $("#modalError .iziModal-header-title").text("Please select another Database" );
            $('#modalError').iziModal('open');
            $("#rundetailTable").hide();
            
        }

    }).done(function () {
        
    });
}
function getClientGroups(client) {
    var groupURL = "http://172.21.132.79/api/client/" + client + "/group/";
    $.get(groupURL, function (data) {
        if (data.length > 0) {
            $.each(data, function(index, value){
                $("#runGroupValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields\" data-id=\"' + data[index].groupID + '\">' + data[index].group_name + '</div></div>');
            });  
            $("#clientFail").hide();
            $("#clientDone").fadeIn();
        } else {
            $("#rundetailDatabaseSelect option").not(".default").remove();
            $("#modalError .iziModal-header-title").text("Please select another client." );
            $('#modalError').iziModal('open');
            $("#clientDone, #databaseDone, #databaseFail").hide();
            $("#clientFail").fadeIn();
            
            
        }

    }).done(function () {
        $("#runGroupValues").find(".fieldRow").each(function() {
            $(this).click(function() {
                $(this).toggleClass("selectedGroup");
            });
        });

    });
}
function getClientRunDetailsManage(database) {
    var runDetailURL = "http://172.21.132.79/api/database/" + database + "/RunDetail"

    $.get(runDetailURL, function (data) {
        if (data.length > 0) {
            $.each(data, function(index, value){
                $("#runDetailValues").append('<div class=\"fieldRow activeRows\"><div class=\"fields\" data-id=\"' + data[index].runDetailID + '\">' + data[index].table_name + '</div></div>');
            });
            $("#runDetailValues").find(".fieldRow").each(function() {
                $(this).click(function() {
                    $(this).toggleClass("selectedGroup");
                });
            });
            $(".hideLinks").fadeIn();
            $("#databaseFail").hide();
            $("#databaseDone").fadeIn();

        } else {
            $("#modalError .iziModal-header-title").text("Please select another database." );
            $('#modalError').iziModal('open');
            $("#databaseDone").hide();
            $("#databaseFail").fadeIn();
        }
    });
//        .done(function () {
//        
//    });                               
}

function processGroup(target) {
    var selectedGroupNames = $("#runGroupValues").find(".selectedGroup");  
    var selectedGroupNamesLength = $("#runGroupValues").find(".selectedGroup").each(function(){return $(this);});
    var selectedRunDetailsLength = $("#runDetailValues").find(".selectedGroup").each(function(){return $(this);});  
    var groupXruns = selectedGroupNamesLength.length * selectedRunDetailsLength.length;

    var selectedRunDetails = $("#runDetailValues").find(".selectedGroup");
    var countSubmissions = 0;

    var completedArray = [];
    var errorArray = [];

    if (groupXruns > 0) {
        selectedGroupNames.each(function() {
            var groupNameValue = $(this).find(".fields").data("id");
            selectedRunDetails.each(function(index) {
                var rundetailValues = $(this).find(".fields").data("id");

                 $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    url: 'http://172.21.132.79/api/rundetail/group',
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify({
                        runDetailID: rundetailValues,
                        GroupID: groupNameValue
                    }),
                    error: function (e) {
                        countSubmissions++
                        console.log(e);
                        errorArray.push({
                            runDetailID : rundetailValues,
                            GroupID : groupNameValue
                        });
                        $.each(errorArray, function(index) {
                            console.log('hello');
                            $("#failedGroups").append(
                                '<div class=\"justBottomBorder successError\">' +
                                    '<div class=\"col-sm-6 col-xs-12\">'
                                         + "runDetailID : " +  errorArray[index].runDetailID +
                                    '</div>'+
                                    '<div class=\"col-sm-6 col-xs-12\">'
                                         + "GroupID : " +  errorArray[index].GroupID +
                                    '</div>' +
                                '</div>'
                            );
                        });
                        $("#modalAddGroup").iziModal('stopLoading');


                    },
                    beforeSend: function () {
                        $(".successError").remove(); 
                        $("#modalAddGroup").iziModal('open');

                    },
                    complete: function () {},
                    success: function (result) {
                        countSubmissions++
                        console.log(result.runDetailGroupID + " was created!");
                        completedArray.push(result);

                        $.each(completedArray, function(index) {
                            $("#createdGroups").append(
                                '<div class=\"justBottomBorder successError\">' +
                                    '<div class=\"col-sm-4 col-xs-12\">'
                                        + "runDetailGroupID : " +  completedArray[index].runDetailGroupID +
                                    '</div>' +
                                    '<div class=\"col-sm-4 col-xs-12\">'
                                        + "runDetailID : " +  completedArray[index].runDetailID +
                                    '</div>' +
                                    '<div class=\"col-xs-4 col-xs-12\">'
                                        + "GroupID : " +   completedArray[index].GroupID +
                                    '</div>' +
                                '</div>'
                            );
                        });
                        $("#modalAddGroup").iziModal('stopLoading');

                    }
                });
            });
        });

    }else {
        $("#modalError .iziModal-header-title").text("Please select from both tables." );
        $('#modalError').iziModal('open');
    }   

}