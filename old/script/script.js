$(document).ready(function () {

    //////////////////////////////////////////////
    ///////////// GLOBAL INITS ///////////////////
    //////////////////////////////////////////////
    var init = true;
    getClients(init = true, after = false);
    var init = false;
    $('#addNewScope').each(function () {
        $(this).click(function () {
            var _this_ = $(this);
            var parente = $(this).parent().children().not('[type=submit], div').length;
            var thisData = $(this).attr('data-postType');
            var thisURL = 'http://172.21.132.79/api/scope';
            var data = _this_.parent().parent().find('input[type=text], select');

            var allVals = {};

            data.each(function () {

                var id = $(this).attr('id');
                var values = $(this).val();
                allVals[id] = values;
            });

            var stringedData = JSON.stringify(allVals);


            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                url: thisURL,
                type: 'POST',
                dataType: "json",
                contentType: "application/json",
                data: stringedData,
                error: function (e) {
                    console.log(e);

                    $(updated)
						.text('Could Not Update')
						.hide()
						.attr('display', 'block')
						.outerWidth('150')
						.attr('color', 'red');
                    $(updated).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
                },
                beforeSend: function () {
                    $('.cont1').fadeOut(100);
                    $('.relDiv, #cloud').show();
                    $('#loader').fadeIn(100);

                },
                complete: function () {
                    data.not('select').val('');
                    var selectOnly = data.not('input[type=text]');
                    $('.clientScope').val('default');


                },
                success: function (result) {

                    console.log(result);
                    $('#modalReason').empty();
                    $('.relDiv, #cloud').delay(2000).fadeOut(300);
                    setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                    setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                    $('#loader').delay(4000).fadeOut();
                }
            });
        });
    });
    $("#modal1").click(function (event) {
        event.stopPropagation();
    });
    $(document).click(function () {
        $('#modalfull').click(function () {
            $('.cont1').fadeOut(500);
            $('#modalReason').empty();
            $('#sortable2').empty();
            $('.databaseClass').val('default');
            $('.RunDeets').fadeOut(200);
        });
    });
    $('.addType').each(function () {
        $(this).click(function () {
            var _this_ = $(this);
            var childRen = _this_.parent().children().not('.refreshList, .addType, .isID').length;
            var childRenText = _this_.parent().children().not('.refreshList, .addType, .isID');
            var postType = _this_.parent().parent().find('.searchDiv:first').attr('id');
            var postFields = _this_.parent().parent().find('.bridge:first').find('.editAble');
            var postId = [];
            var childRenTextArr = [];

            $.each(postFields, function () {
                postId.push($(this).attr('id'));
            });
            $.each(childRenText, function () {
                childRenTextArr.push($(this).text());
            });

            var fieldCount = 0;

            if (postType == 'Database') {
                $("<select>")
					.attr('id', 'clientID')
					.addClass('ClientDatabase')
					.attr('data-postType', postType)
					.appendTo($('#modalReason'));
                $('.ClientDatabase').before('<div class=\"headerText\">Client<span style=\"color:red;\">*</span></div>');

                var where = $('.ClientDatabase');

                $("<select>")
					.attr('id', 'serverInfoID')
					.addClass('ServerDatabase')
					.attr('data-postType', postType)
					.appendTo($('#modalReason'));
                $('.ServerDatabase').before('<div class=\"headerText\">Server<span style=\"color:red;\">*</span></div>');

                var whereSecond = $('.ServerDatabase');

                getClients(init = false, after = true, where, whereSecond)

                where = '';
                whereSecond = '';
            }

            for (i = 0; i < childRen; i++) {
                fieldCount++;
                $("<input>")
				.attr('type', 'text')
				.attr('id', postId[i] + 'XxX')
				.attr('placeholder', 'Type ' + postId[i] + ' Here')
				.appendTo($('#modalReason'));
                $('#modalReason').find('#' + postId[i] + 'XxX').before('<div class=\"headerText\">' + childRenTextArr[i] + '<span style=\"color:red;\">*</span></div>');
                if (fieldCount === childRen) {
                    $("<input>")
					.attr('type', 'submit')
					.attr('id', 'ConfigSubmit')
					.attr('data-postType', postType)
					.attr('value', 'Submit ' + postType)
					.appendTo($('#modalReason'));
                    $('#ProjectXxX').after('<div id=\"tickboxwrap\"><input type=\"checkbox\" id=\"tableYes\" /><span class=\"toggleChecked\">Alternative Table Catalog Name </span></div>');
                    $('#Table_CatalogXxX').prev('div').addClass('addedClass');

                    $('#Table_CatalogXxX, .addedClass').wrapAll('<div id=\"slideCatalog\"></div>');

                    $('#slideCatalog').slideUp();
                    $('#tableYes').click(function () {
                        $('#slideCatalog').slideToggle();
                        var checkBox = $(this);
                        if (!checkBox.is(':checked')) {
                            $('#Table_CatalogXxX').val($('#ProjectXxX').val());

                        }
                    });
                    $('#ProjectXxX').focusout(function () {
                        var checkBox = $('#tableYes');
                        if (!checkBox.is(':checked')) {
                            $('#Table_CatalogXxX').val($('#ProjectXxX').val());

                        } else {

                        }
                    });
                }
            }

            $('.cont1').fadeIn(500);
            $('#ConfigSubmit').each(function () {
                $(this).click(function () {
                    var _this_ = $(this);
                    var parente = $(this).parent().children().not('[type=submit], div').length;
                    var thisData = $(this).attr('data-postType');
                    var thisURL = "http://172.21.132.79/api/" + thisData;
                    var data = $(this).parent().find('input[type=text], select');

                    var allVals = {};

                    data.each(function () {
                        var idReplaced = $(this).attr('id').replace('XxX', '');
                        var id = idReplaced;

                        var values = $(this).val();
                        allVals[id] = values;

                    });

                    var stringedData = JSON.stringify(allVals);


                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        url: thisURL,
                        type: 'POST',
                        dataType: "json",
                        contentType: "application/json",
                        data: stringedData,
                        error: function (e) {
                            console.log(e);

                            $(updated)
								.text('Could Not Update')
								.hide()
								.attr('display', 'block')
								.outerWidth('150')
								.attr('color', 'red');
                            $(updated).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
                        },
                        beforeSend: function () {
                            $('.cont1').fadeOut(100);
                            $('.relDiv, #cloud').show();
                            $('#loader').fadeIn(100);

                        },
                        complete: function () {

                        },
                        success: function (result) {

                            console.log(result);
                            $('#modalReason').empty();
                            $('.relDiv, #cloud').delay(2000).fadeOut(300);
                            setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                            setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                            $('#loader').delay(4000).fadeOut();
                        }
                    });
                });
            });
        });
    });


    //////////////////////////////////////////////
    /////////////CONFIGURATION PAGE///////////////
    //////////////////////////////////////////////

    $('select.client').change(function () {

        var clientVal = $(this).val();
        var idTableVal = $('#tableVal').text();

        if (idTableVal != clientVal) {
            $('#deleteClientTable').remove();
        }

        tableGetClients();
        $('#deleteDatabaseTable').remove();
        $('#deleteServerTable').remove();

        var clientIDnow = $(this).val();
        var clientServerID = "http://172.21.132.79/api/client/" + clientIDnow + "/server/";
        var thisSelect = $(this);
        var clientCont = $(this).parent();

        //  Gets servers and appends options depending on the amount of unique options that are shown
        $.get(clientServerID, function (data) {

            var selectServer = $(thisSelect).parent().parent().find('select.server');
            var selectOption = selectServer.find('option');
            var OptionNotDisabled = $(thisSelect).parent().parent().find('select.server option').not(':disabled');
            var selectDatabase = $(thisSelect).parent().parent().find('select.database');
            var OptionNotDisabledDB = $(selectDatabase).find('option').not(':disabled');
            var checkArray = [];
            selectDatabase.val('default');
            OptionNotDisabled.remove();
            OptionNotDisabledDB.remove();

            for (i = 0; i < data.length; i++) {

                if (jQuery.inArray(data[i].serverInfoID, checkArray) !== -1) {

                } else {
                    $(selectServer).append($('<option>').text(data[i].Linked_Server).attr('value', data[i].serverInfoID));
                    checkArray.push(data[i].serverInfoID);

                }
            }
            checkArray = [];
        })
			.done(function () {
			    $('.server').val('default');


			});


    });

    $('select.server').change(function () {
        console.log('changed');
        var ServerVal = $(this).val();
        var idtableServerVal = $('#tableServerVal').text();
        $('#deleteDatabaseTable').remove();
        if (idtableServerVal != ServerVal || $('#tableServerVal').length > 0) {

            $('#deleteServerTable').remove();

        }

        tableGetServer(ServerVal);


        var serverIDnow = $(this).val();
        var clientDBID = "http://172.21.132.79/api/server/" + serverIDnow + "/database/";
        var thisSelect = $(this);
        var findDB = $(this).parent().find('.database');
        var serverCont = $(this).parent();

        $.get(clientDBID, function (data) {

            var selectDatabase = $(thisSelect).parent().parent().find('select.database');
            var selectClient = $(thisSelect).parent().parent().find('select.client').val();
            var OptionNotDisabledDB = $(selectDatabase).find('option').not(':disabled');
            selectDatabase.val('default');
            OptionNotDisabledDB.remove();

            for (i = 0; i < data.length; i++) {

                if (data[i].clientID == selectClient) {
                    $(selectDatabase).append($('<option>').text(data[i].Project).attr('value', data[i].DBinfoID));

                }
            }

        });
    });

    $('select.database').change(function () {
        var databaseVal = $(this).val();
        var idtableDatabaseVal = $('#tableDatabaseVal').text();


        if (idtableDatabaseVal != databaseVal || $('#tableDatabaseVal').length > 0) {
            $('#deleteDatabaseTable').remove();

        }
        var serverValue = $('.server').val();
        tableGetDatabase(databaseVal);



    });

    //////////////////////////////////////////////
    //////////CONFIGURATION PAGE GETS/////////////
    //////////////////////////////////////////////

    function tableGetClients() {

        $.get("http://172.21.132.79/api/client", function (data) {

            var clientList = data;
            var clientVal = $('select.client').val();

            for (i = 0; i < clientList.length; i++) {

                if (clientList[i].clientID == clientVal) {
                    $('.client').after('<div id=\'deleteClientTable\'><div class="tableWrapS"><table class="table"><tr class=\"headerRow\"><td id=\"\" >clientID</td><td >client_name</td><td >EAGLE_client_name</td><td >EAGLE_server</td><td >client_display_name</td></tr><tr><td id=\"tableVal\" class=\"inputClientID clientIDUpdate\" >' + data[i].clientID + '</td><td class=\"inputABLE\" id=\"clientNameUpdate\">' + data[i].client_name + '</td><td class=\"inputABLE\" id=\"eagleClientNameUpdate\">' + data[i].EAGLE_client_name + '</td><td class=\"inputABLE\" id=\"eagleServer\" >' + data[i].EAGLE_server + '</td><td class=\"inputABLE\" id=\"clientDisplayName\" >' + data[i].client_display_name + '</td></tr> <tr><td id=\"\" colspan=\"5\" class=\"editButtons\" align=\"right\" ></td></tr></table></div><table width="100%"><tr><td><div class=\"editCont\"><div id=\"updated\"></div><div id=\"refreshed\"></div><div class=\"editCont\"><div class=\"editOptionUpdate\"><i class=\"fa fa-check\"></i></div><div class=\"editOptionReload\"><i class=\"fa fa-refresh\"></i></div></div></td></tr></table></div>');

                    $('.table .inputABLE').each(function () {

                        if ($(this).text() == 'null') {
                            $(this).empty();
                        }
                    });
                    $("td.inputABLE").dblclick(function () {
                        newInput(this);
                    });

                    function closeInput(elm) {
                        var value = $(elm).find('input').val();
                        $(elm).empty().text(value);

                        $(elm).bind("dblclick", function () {
                            newInput(elm);
                        });
                    }

                    function newInput(elm) {
                        $(elm).unbind('dblclick');

                        var value = $(elm).text();
                        $(elm).empty();

                        $("<input>")
							.attr('type', 'text')
							.attr('class', 'inputABLE')
							.val(value)
							.blur(function () {
							    closeInput(elm);
							})
							.appendTo($(elm))
							.focus();
                    }
                    $('.editOptionUpdate').click(function () {

                        var one = $('.clientIDUpdate').text();
                        var two = $('#clientNameUpdate').text();
                        var three = $('#eagleClientNameUpdate').text();
                        var four = $('#eagleServer').text();
                        var five = $('#clientDisplayName').text();
                        var thisReloadButton = $(this).parent().parent().find('#updated');

                        updateValues(one, two, three, four, five, thisReloadButton);

                    });
                    $('.editOptionReload').click(function () {
                        var one = $('.clientIDUpdate').text();
                        var thisReloadButton = $(this).parent().parent().find('#refreshed');
                        refreshValues(one, thisReloadButton);
                    });


                }

            }
        });
    }


    function tableGetServer(clientServer) {
        var serverURL = 'http://172.21.132.79/api/server/' + clientServer;
        $.get(serverURL, function (data) {

            var serverData = data;
            var serverVal = $('select.server').val();

            $('.server').after('<div id=\"deleteServerTable\"><div class="tableWrapS"><table class="serverTable"><tr class=\"headerRow\"><td >serverInfoID</td><td >Linked_Server</td><td >IP_address</td><td >server_type</td></tr><tr><td id=\"tableServerVal\" class=\"inputClientID serverInfoIDCont1\" >' + data.serverInfoID + '</td><td class=\"inputABLE\" id=\"Linked_Server\">' + data.Linked_Server + '</td><td class=\"inputABLE\" id=\"IP_address\">' + data.IP_address + '</td><td class=\"inputABLE\" id=\"server_type\" >' + data.server_type + '</td></tr> <tr><td id=\"\" colspan=\"6\" class=\"editButtons\" align=\"right\" ></td></tr></table></div><table width="100%"><tr><td><div class=\"editCont\"><div class=\"editCont\"><div id=\"updated\">Could Not Update</div><div id=\"refreshed\">Could Not Refresh</div><div class=\"editCont\"><div class=\"editServerOptionUpdate\"><i class=\"fa fa-check\"></i></div><div class=\"editServerOptionReload\"><i class=\"fa fa-refresh\"></i></div></div></td></tr></table></div>');
            var textt = $('serverTable.inputABLE').text();

            $('.serverTable .inputABLE').each(function () {

                if ($(this).text() == 'null') {
                    $(this).empty();
                }
            });

            $(".serverTable td.inputABLE").dblclick(function () {
                newInput(this);
            });

            function closeInput(elm) {
                var value = $(elm).find('input').val();
                $(elm).empty().text(value);

                $(elm).bind("dblclick", function () {
                    newInput(elm);
                });
            }

            function newInput(elm) {
                $(elm).unbind('dblclick');

                var value = $(elm).text();
                $(elm).empty();

                $("<input>")
					.attr('type', 'text')
					.attr('class', 'inputABLE')
					.val(value)
					.blur(function () {
					    closeInput(elm);
					})
					.appendTo($(elm))
					.focus();
            }
            $('.editServerOptionUpdate').click(function () {
                var one = $('.serverInfoIDCont1').text();
                var two = $('#Linked_Server').text();
                var three = $('#IP_address').text();
                var four = $('#server_type').text();
                var updateButton = $(this).parent().parent().find('#updated');
                updateServerValues(one, two, three, four, updateButton);

            });
            $('.editServerOptionReload').click(function () {
                var refreshButton = $(this).parent().parent().find('#refreshed');
                var one = $('.serverInfoIDCont1').text();
                refreshServerValues(one, refreshButton);
            });

        });
    }
    function tableGetDatabase(databaseID) {

        var databaseURL = 'http://172.21.132.79/api/database/' + databaseID;

        $.get(databaseURL, function (data) {

            $('.database').after('<div id=\"deleteDatabaseTable\"><div class="tableWrapS"><table class="databaseTable"><tr class=\"headerRow\"><td id=\"\" >DBinfoID</td><td >serverInfoID</td><td >clientID</td><td >Project</td><td >Table_Catalog</td><td>Process_Validation</td></tr><tr><td id=\"tableVal\" class=\"inputClientID Database_DBinfoID\" >' + data.DBinfoID + '</td><td class=\"\" id=\"Database_serverInfoID\">' + data.serverInfoID + '</td><td class=\"\" id=\"Database_clientID\">' + data.clientID + '</td><td class=\"inputABLE\" id=\"Database_Project\" >' + data.Project + '</td><td class=\"inputABLE\" id=\"Database_Table_Catalog\" >' + data.Table_Catalog + '</td><td class=\"inputABLE\" id=\"Database_Process_Validation\" >' + data.Process_Validation + '</td></tr> <tr><td id=\"\" colspan=\"6\" class=\"editButtons\" align=\"right\" ></td></tr></table></div><table width="100%"><tr><td><div class=\"editCont\"><div class=\"editCont\"><div id=\"updated\"></div><div id=\"refreshed\"></div><div class=\"editCont\"><div class=\"editDatabaseOptionUpdate\"><i class=\"fa fa-check\"></i></div><div class=\"editDatabaseOptionReload\"><i class=\"fa fa-refresh\"></i></div></div></td></tr></table></div>');

            $('.databaseTable .inputABLE').each(function () {

                if ($(this).text() == 'null') {
                    $(this).empty();
                }
            });

            $(".databaseTable td.inputABLE").dblclick(function () {
                newInput(this);
            });

            function closeInput(elm) {
                var value = $(elm).find('input').val();
                $(elm).empty().text(value);
                $(elm).empty().text(value);

                $(elm).bind("dblclick", function () {
                    newInput(elm);
                });
            }

            function newInput(elm) {
                $(elm).unbind('dblclick');

                var value = $(elm).text();
                $(elm).empty();

                $("<input>")
					.attr('type', 'text')
					.attr('class', 'inputABLE')
					.val(value)
					.blur(function () {
					    closeInput(elm);
					})
					.appendTo($(elm))
					.focus();
            }
            $('.editDatabaseOptionUpdate').click(function () {
                var one = $('.Database_DBinfoID').text();
                var two = $('#Database_serverInfoID').text();
                var three = $('#Database_clientID').text();
                var four = $('#Database_Project').text();
                var five = $('#Database_Table_Catalog').text();
                var six = $('#Database_Process_Validation').text();
                var updateButton = $(this).parent().parent().find('#updated');
                updateDatabaseValues(one, two, three, four, five, six, updateButton);

            });
            $('.editDatabaseOptionReload').click(function () {
                var refreshButton = $(this).parent().parent().find('#refreshed');
                var one = $('.Database_DBinfoID').text();
                console.log(one);
                refreshDatabaseValues(one, refreshButton);
            });

        });
    }



    function updateValues(client, clientName, eagleClientName, eagleServer, clientDisplayName, thisOne) {
        var clientUrl = 'http://172.21.132.79/api/client/' + client;
        var thisIsIt = thisOne;
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: clientUrl,
            type: 'PUT',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                clientID: client,
                client_name: clientName,
                EAGLE_client_name: eagleClientName,
                EAGLE_server: eagleServer,
                client_display_name: clientDisplayName
            }),
            error: function (e) {
                console.log(e);
                $(thisIsIt)
					.text('Could Not Update')
					.hide()
					.attr('display', 'block')
					.outerWidth('150')
					.attr('color', 'red');
                $('#updated').delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
            },
            beforeSend: function () {
                $('#loader').fadeIn(100);
            },
            complete: function () {
                $('#contThree').find('[type="text"]').val(null);
                $('#loader').delay(3000).fadeOut();
            },
            success: function (result) {
                console.log(result);
                $(thisIsIt)
					.text('Updated')
					.hide()
					.attr('display', 'block')
					.attr('color', 'green');
                $('#updated').delay(3000).fadeIn(1000).delay(3000).fadeOut(1000);

            }
        });

    }
    function refreshValues(client, thisOne) {
        var serverURL = "http://172.21.132.79/api/client/" + client;
        var thisIsIt = thisOne;
        $.get(serverURL, function (data) {

            var two = $('#clientNameUpdate');
            var three = $('#eagleClientNameUpdate');
            var four = $('#eagleServer');
            var five = $('#clientDisplayName');

            two.text(data.client_name);
            three.text(data.EAGLE_client_name);
            four.text(data.EAGLE_server);
            five.text(data.client_display_name);
        })
			.done(function () {
			    $(thisIsIt)
					.hide()
					.text('Refreshed')
					.attr('display', 'block')
					.attr('color', 'green');
			    $('#refreshed').delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

			})
			.fail(function () {
			    $(thisIsIt)
					.hide()
					.text('Could Not Refresh')
					.attr('display', 'block')
					.attr('color', 'red')
					.outerWidth('150');
			    $('#refreshed').delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

			})

    }
    function updateServerValues(serverInfoID, Linked_Server, IP_address, server_type, thisOne) {

        var thisIsIt = thisOne;
        var serverUrl = 'http://172.21.132.79/api/server/' + serverInfoID;
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: serverUrl,
            type: 'PUT',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                serverInfoID: serverInfoID,
                Linked_Server: Linked_Server,
                IP_address: IP_address,
                server_type: server_type,
            }),
            error: function (e) {
                console.log(e);
                $(thisIsIt)
					.text('Could Not Update')
					.hide()
					.attr('display', 'block')
					.outerWidth('150')
					.attr('color', 'red');
                $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
            },
            beforeSend: function () {
                $('#loader').fadeIn(100);
            },
            complete: function () {
                $('#contThree').find('[type="text"]').val(null);
                $('#loader').delay(3000).fadeOut();
            },
            success: function (result) {
                console.log(result);
                $(thisIsIt)
					.text('Updated')
					.hide()
					.attr('display', 'block')
					.attr('color', 'green');
                $(thisIsIt).delay(3000).fadeIn(1000).delay(3000).fadeOut(1000);

            }
        });

    }
    function refreshServerValues(server, thisOne) {

        var thisIsIt = thisOne;
        var serverURL = "http://172.21.132.79/api/server/" + server;
        $.get(serverURL, function (data) {

            var two = $('#Linked_Server');
            var three = $('#IP_address');
            var four = $('#server_type');

            if (data.Linked_Server == null || data.Linked_Server == '' || data.Linked_Server == 'null') {
                two.text('');
            } else {
                two.text(data.Linked_Server);
            }
            if (data.IP_address == null || data.IP_address == '' || data.IP_address == 'null') {
                three.text('');
            } else {
                three.text(data.IP_address);
            }
            if (data.server_type == null || data.server_type == '' || data.server_type == 'null') {
                four.text('');
            } else {
                four.text(data.server_type);
            }

        })
			.done(function () {
			    $(thisIsIt)
					.hide()
					.text('Refreshed')
					.attr('display', 'block')
					.attr('color', 'green');
			    $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);


			})
			.fail(function () {
			    $(thisIsIt)
					.hide()
					.text('Could Not Refresh')
					.attr('display', 'block')
					.attr('color', 'red')
					.outerWidth('150');
			    $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

			})

    }
    function updateDatabaseValues(DBinfoID, serverInfoID, clientID, Project, Table_Catalog, Process_Validation, thisOne) {

        var databaseUrl = 'http://172.21.132.79/api/database/' + DBinfoID;

        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: databaseUrl,
            type: 'PUT',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                DBinfoID: DBinfoID,
                serverInfoID: serverInfoID,
                clientID: clientID,
                Project: Project,
                Table_Catalog: Table_Catalog,
                Process_Validation: Process_Validation
            }),
            error: function (e) {
                console.log(e);
                $(thisOne)
					.text('Could Not Update')
					.hide()
					.attr('color', 'red')
                    .attr('display', 'block')
					.outerWidth('150');

                $(thisOne).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
            },
            beforeSend: function () {
                $('#loader').fadeIn(100);
            },
            complete: function () {
                $('#contThree').find('[type="text"]').val(null);
                $('#loader').delay(3000).fadeOut();
            },
            success: function (result) {
                //				console.log(result);
                $(thisOne)
					.text('Updated')
					.hide()
					.attr('display', 'block')
					.attr('color', 'green');
                $(thisOne).delay(3000).fadeIn(1000).delay(3000).fadeOut(1000);

            }
        });

    }
    function refreshDatabaseValues(database, thisOne) {

        var thisIsIt = thisOne;
        var databaseUrl = 'http://172.21.132.79/api/database/' + database;
        $.get(databaseUrl, function (data) {

            var one = $('#Database_DBinfoID');
            var two = $('#Database_serverInfoID');
            var three = $('#Database_clientID');
            var four = $('#Database_Project');
            var five = $('#Database_Table_Catalog');
            var six = $('#Database_Process_Validation');

            one.text(data.DBinfoID);
            two.text(data.serverInfoID);
            three.text(data.clientID);
            four.text(data.Project);
            five.text(data.Table_Catalog);
            six.text(data.Process_Validation);

        })
			.done(function () {
			    $(thisIsIt)
					.hide()
					.text('Refreshed')
					.attr('display', 'block')
					.attr('color', 'green');
			    $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

			})
			.fail(function () {
			    $(thisIsIt)
					.hide()
					.text('Could Not Refresh')
					.attr('display', 'block')
					.attr('color', 'red')
					.outerWidth('150');
			    $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

			});

    }

    //////////////////////////////////////////////
    //////////////GROUP PAGE SUBMIT///////////////
    //////////////////////////////////////////////
    $('#addGroupSubmit').click(function () {
        var group_name = $('#group_name').val();
        var groupClientID = $('.groupClientID').val();
        var group_desc = $('#group_desc').val();
        var group_type = $('#group_type').val();
        var display_name = $('#display_name').val();
        //console.log("Client ID: " + groupClientID, group_name, group_desc, group_type, display_name," Login User: " + LoginUser);
        //console.log("Client ID: " + groupClientID, group_name, group_desc, group_type, display_name," Login User: " + LoginUser);
        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: 'http://172.21.132.79/api/group',
            type: 'POST',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                clientID: groupClientID,
                group_name: group_name,
                group_desc: group_desc,
                group_type: group_type,
                display_name: display_name
            }),
            error: function (e) {
                console.log(e);
            },
            beforeSend: function () {
                $('#loader').fadeIn(100);
                $('.relDiv, #cloud').show();

            },
            complete: function () {
                $('#contFive').find('[type="text"]').val(null);
                $('.groupClientID').val('default');
                $('#loader').delay(3000).fadeOut(500);
            },
            success: function (result) {
                $('.relDiv, #cloud').delay(2000).fadeOut(300);
                setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                $('#loader').delay(4000).fadeOut();
            }
        });
    });

    //////////////////////////////////////////////
    ////// ASSOCIATION PAGE INIT FUNCTIONS ///////
    //////////////////////////////////////////////
    $('.RunDeets').hide();
    $('.groupSection').hide();

    //////////////////////////////////////////////
    ///////// ASSOCIATION PAGE FUNCTIONS /////////
    //////////////////////////////////////////////
    $('select.clientClass').change(function () {
        var clientID = $(this).val();
        var databaseClassNotDisabled = $('.databaseClass option').not(':disabled');
        var groupClassNotDisabled = $('.groupClass option').not(':disabled');
        databaseClassNotDisabled.remove();
        groupClassNotDisabled.remove();
        $('.groupSection').hide();
        $('.databaseClass').val('default');
        $('.groupClass').val('default');
        $('#selectedGroup').empty();
        $('#selectedGroupID').empty();
        $('#selectedGroupDesc').empty();
        $('#selectedGroupType').empty();
        $('.RunDeets').fadeOut();
        $('#sortable2 li').remove();
        getClientDatabase(clientID);

    });

    $('select.databaseClass').change(function () {
        $('.groupSection').fadeOut(200);
        $('.RunDeets').fadeOut(200);
        $('.groupSection').hide();
        var databaseID = $(this).val();
        var clientID = $(this).parent().find('.clientClass').val();
        getClientRunDetails(databaseID);
        getClientGroups(clientID);
        $('#sortable2 li').remove();
        $('#sortable1 li').remove();
        $('#selectedGroup').empty();
        $('#selectedGroupID').empty();
        $('#selectedGroupDesc').empty();
        $('#selectedGroupType').empty();
        $('.groupClass').val('default');
        var groupClassNotDisabled = $('.groupClass option').not(':disabled');
        groupClassNotDisabled.remove();
        $('.clientClass option').each(function () {
            if ($(this).val() == 'default') {
                $(this).attr('disabled', 'disabled');
            } else {
                $(this).removeAttr('disabled');
            }

        });
    });

    $('select.groupClass').change(function () {
        var thisID = $(this).val();
        var groupURL = "http://172.21.132.79/api/group/" + thisID;
        $.get(groupURL, function (data) {

            $('#selectedGroup').text(data.group_name);
            $('#selectedGroupID').text(data.groupID);
            $('#selectedGroupDesc').text(data.group_desc);
            $('#selectedGroupType').text(data.group_type);

        });
        $('.groupSection').fadeIn(200);
    });

    $('#YesAssocButton').click(function () {
        $('.cont').hide();
        $('#hideForNew').hide();
        $('#hideForNew').addClass('dontValidate');
        $('#hideForNew').after('<div id="runDetailsAdded"><div class=\"headerText\">Run Detail Display Name</div><input type=\"text\" class=\"addRunDetails\" placeholder=\"Type desired name\" id=\"display_name_run\"><div class=\"headerText\">Aggregation</div><select class=\"aggregation\"><option selected disabled value=\"default\">choose one</option><option value="COUNT">COUNT</option><option value="MAX">MAX</option><option value="MIN">MIN</option><option value="AVG">AVG</option><option value="SUM">SUM</option><option value="Count(*)">Count(*)</option></select><div class=\"headerText\">Table Name</div><select class=\"table_name\"><option selected disabled value=\"default\">choose one</option></select><div class=\"headerText\">Filter (WHERE Clause)</div><input type=\"text\" placeholder="Type desired Filter" id=\"filter\" style="" /><input type=\"text\" id=\"table_schema\" style="display:none;" /><div style=\"float:left;\"><input type=\"submit\" value=\"Add Run Detail\" id=\"addRunDetailSubmit\" /></div></div>');

        var databaseID = $('.databaseManageRuns').val();

        getTables(databaseID);

        function getTables(dbid) {
            var tableURL = "http://172.21.132.79/api/database/" + dbid + "/table";
            $.get(tableURL, function (data) {

                for (i = 0; i < data.length; i++) {

                    $('select.table_name').append($('<option>').text(data[i].TableName).attr('value', data[i].TableName));
                    $('#table_schema').val(data[i].TableName)


                }

            })
                .done(function () {

                    $('#addRunDetailSubmit').click(function () {
                        var table_name = $('.table_name').val();
                        $.get(tableURL, function (data) {

                            for (i = 0; i < data.length; i++) {
                                if (data[i].TableName == table_name) {

                                    $('#table_schema').val(data[i].TableSchema);
                                }
                            }
                        });
                        //                                                                                       
                        var DBinfoID = $('.databaseManageRuns').val();
                        var display_name_run = $('#display_name_run').val();
                        var aggregation = $('.aggregation').val();
                        var table_name = $('.table_name').val();
                        var table_schema = $('#table_schema').val();
                        var filter = $('#filter').val();

                        $.ajax({
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                            url: 'http://172.21.132.79/api/rundetail',
                            type: 'POST',
                            dataType: "json",
                            contentType: "application/json",
                            data: JSON.stringify({
                                DBinfoID: DBinfoID,
                                display_name: display_name_run,
                                aggregation: aggregation,
                                table_name: table_name,
                                table_schema: table_schema,
                                filter: filter
                            }),
                            error: function (e) {
                                console.log(e);
                            },
                            beforeSend: function () {
                                $('.relDiv, #cloud').show();
                                $('#loader').fadeIn(100);
                            },
                            complete: function () {
                                $('#contFive').find('[type="text"]').val(null);
                                $('.groupClientID').val('default');

                            },
                            success: function (result) {

                                $('.relDiv, #cloud').delay(2000).fadeOut(300);
                                setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                                setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                                $('#loader').delay(4000).fadeOut();

                                console.log(result);
                            }
                        })
                        .done(function () {
                            $('.databaseManageRuns').val('default');
                            $('.clientManageRuns').val('default');
                            $('#hideForNew').show();
                            $('#hideForNew').removeClass('dontValidate');
                            $('#runDetailsAdded').remove();


                        });

                    });

                });


        }
    });


    //////////////////////////////////////////////
    ///////// POSTING RUN GROUP //////////////////
    //////////////////////////////////////////////
    $('#addRunGroupSubmit').click(function () {
        var GroupID = $('#selectedGroupID').text();
        var sortedOne = $('#sortable1 li');
        if (sortedOne.length > 0) {
            sortedOne.each(function () {
                var runDetailID = $(this).val();
                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    url: 'http://172.21.132.79/api/rundetail/group',
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify({

                        runDetailID: runDetailID,
                        GroupID: GroupID
                    }),
                    error: function (e) {
                        console.log(e);
                    },
                    beforeSend: function () {
                        $('.relDiv, #cloud').show();
                        $('#loader').fadeIn(100);
                    },
                    complete: function () {
                        $('.groupClientID').val('default');
                        $('.relDiv, #cloud').delay(2000).fadeOut(300);
                        setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                        setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                        $('#loader').delay(4000).fadeOut();

                    },
                    success: function (result) {
                        $('#sortable1 li').each(function () {
                            $(this).appendTo('#sortable2');
                            $('span#PlaceHoldRun').fadeIn(200);
                        });
                        console.log(result);
                    }
                }).done(function () {

                });
            });
        } else {
            alert('Please Assign Run Details Before Submitting.');
        }
    });

    //////////////////////////////////////////////
    ///////// ASSOCIATION PAGE FUNCTIONS /////////
    //////////////////////////////////////////////

    function getClientDatabase(client) {

        var databaseURL = "http://172.21.132.79/api/client/" + client + "/database/";

        $.get(databaseURL, function (data) {
            var checkArrayDB = [];
            for (i = 0; i < data.length; i++) {

                if (jQuery.inArray(data[i].DBinfoID, checkArrayDB) !== -1) {


                } else {

                    $('select.databaseClass').append($('<option>').text(data[i].Project).attr('value', data[i].DBinfoID));
                    checkArrayDB.push(data[i].DBinfoID);

                }
            }
            checkArrayDB = [];

        });
    }

    function getClientRunDetails(database) {
        var runDetailURL = "http://172.21.132.79/api/database/" + database + "/RunDetail";

        $.get(runDetailURL, function (data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {

                    $('ul.runDetailOptions').append($('<li><a><div class="isTable"><div class="isCell">' + data[i].display_name + '</div></div></a></li>').attr('value', data[i].runDetailID));

                }
                $('.RunDeets').fadeIn(200);
            } else {
                //add run details option
            }



        });



    }

    function getClientGroups(client) {

        var groupURL = "http://172.21.132.79/api/client/" + client + "/group/";
        $.get(groupURL, function (data) {

            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    $('select.groupClass').append($('<option>').text(data[i].group_name).attr('value', data[i].groupID));
                }
            } else {
                $('#modalReason').empty();
                $('#modalReason').append('<div>Sorry, no groups were found.<br /><br />Would you like to create a group?</div><div class=\"buttonContainer\"> <div class=\"floatLeft\"><button id=\"YesGroupButton\">Yes</button></div><div class=\"floatRight\"><button id=\"NoGroupButton\">No</button></div></div>');
                $('.cont1').fadeIn(200);
                $('.cont1').attr('data', 'active');
            }




        }).done(function () {

            $('#YesGroupButton').click(function () {

                var groupAddForm = $('.formParentGroup').html();
                var clientVal = $('.clientClass').val();
                $('#modalReason').empty();
                $('#modalReason').append(groupAddForm);

                $('#modalReason .groupClientID').val(clientVal);
                $('#modalReason .groupClientID').attr('disabled', 'disabled');

                modalAddGroup()

            });
            $('#NoGroupButton').click(function () {
                $('.cont1').fadeOut(200);
                $('.cont1').attr('data', 'inactive');
                $('#sortable2').empty();
                $('.databaseClass').val('default');
                $('.RunDeets').fadeOut(200);

            });
        });
    }

    function modalAddGroup() {

        $('#modalReason #addGroupSubmit').click(function () {
            var group_name = $('#modalReason #group_name').val();
            var groupClientID = $('#modalReason .groupClientID').val();
            var group_desc = $('#modalReason #group_desc').val();
            var group_type = $('#modalReason #group_type').val();
            var display_name = $('#modalReason #display_name').val();
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                url: 'http://172.21.132.79/api/group',
                type: 'POST',
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    clientID: groupClientID,
                    group_name: group_name,
                    group_desc: group_desc,
                    group_type: group_type,
                    display_name: display_name
                }),
                error: function (e) {
                    console.log(e);
                },
                beforeSend: function () {
                    $('.cont1').fadeOut(200);
                    $('.cont1').attr('data', 'inactive');
                    $('#loader').fadeIn(100);

                },
                complete: function () {
                    $('.databaseClass').val('default');
                    $('#modalReason').empty();
                    $('#contFive').find('[type="text"]').val(null);
                    $('.groupClientID').val('default');
                    $('#loader').delay(3000).fadeOut(500);

                },
                success: function (result) {
                    console.log(result);
                    setTimeout(function () { alert('Success! Database was added.'); }, 3600);
                }
            });
        });
    }


    //////////////////////////////////////////////
    ///////// ASSOCIATION PAGE SORTING ///////////
    //////////////////////////////////////////////
    $(function () {
        $('#PlaceHoldRun2').hide();
        var count = 0;
        $("#sortable1").sortable({
            connectWith: ".connectedSortable",
            items: 'li',
            start: function (event, ui) {
                clone = $(ui.item[0].outerHTML).clone();

            },
            stop: function (event, ui) {
                if ($('ul#sortable1 li').length == 0) {
                    $('span#PlaceHoldRun').fadeIn(200);

                } else {
                    $('#RemoveButton').show();
                    $('span#PlaceHoldRun').fadeOut(200);
                    $('span#PlaceHoldRun2').fadeOut(200);
                }
                if ($('ul#sortable1 li').length == 0) {
                    $('span#PlaceHoldRun').fadeIn(200);
                }
            },
            beforeStop: function (event, ui) {

            },
            placeholder: {
                element: function (clone, ui) {
                    return $('<li class="selected">' + clone[0].innerHTML + '</li>');
                },
                update: function () {
                    return;
                }
            },
            over: function (event, ui) {



            }

        }).disableSelection();

        $("#sortable2").sortable({
            connectWith: ".connectedSortable",
            items: 'li',
            start: function (event, ui) {
                clone = $(ui.item[0].outerHTML).clone();
            },
            stop: function (event, ui) {
                if ($('ul#sortable1 li').length == 0) {
                    $('span#PlaceHoldRun').fadeIn(200);

                } else {
                    $('span#PlaceHoldRun').fadeOut(200);
                    $('ul#sortable1').css('height', 'auto');
                }
                if ($('ul#sortable2 li').length == 0) {

                    $('span#PlaceHoldRun2').fadeIn(200);

                } else {

                }


                $('ul#sortable1').removeClass('activePlaceholder');
            },
            placeholder: {
                element: function (clone, ui) {
                    return $('<li class="selected">' + clone[0].innerHTML + '</li>');
                },
                update: function () {
                    return;
                }
            }

        }).disableSelection();



    });
    $(function ($) {
        jQuery.expr[':'].Contains = function (a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };

        function listFilter(header, list) {
            var form = $("<form>").attr({
                "class": "filterform",
                "action": "#"
            }),
                input = $("<input>").attr({
                    "class": "filterinput",
                    "type": "text",
                    "placeholder": "Search"
                });
            $(form).append(input).appendTo(header);

            $(input)
                .change(function () {
                    var filter = $(this).val();
                    if (filter) {
                        $(list).find("a:not(:Contains(" + filter + "))").parent().slideUp();
                        $(list).find("a:Contains(" + filter + ")").parent().slideDown();
                    } else {
                        $(list).find("li").slideDown();
                    }
                    return false;
                })
                .keyup(function () {
                    $(this).change();
                });
        }

        $(function () {
            listFilter($("#headerList"), $(".list"));
        });
    }(jQuery));

    //////////////////////////////////////////////
    ///////// MANAGING RUN INITS /////////////////
    //////////////////////////////////////////////
    $('.cont').hide();
    $('.cont').removeAttr('display');

    $("#modal").click(function (event) {
        event.stopPropagation();
    });
    $(document).click(function () {
        $('#modalfull').click(function () {
            $('.cont').fadeOut(500);
            $('.databaseManageRuns').val('default');

        });
        $('#NoAssocButton').click(function () {
            $('.cont').fadeOut(500);
            $('.databaseManageRuns').val('default');
        });

    });

    //////////////////////////////////////////////
    ///////// MANAGING RUN PAGE //////////////////
    //////////////////////////////////////////////
    $('select.clientManageRuns').change(function () {
        var clientID = $(this).val();
        var OptionNotDisabled = $('select.databaseManageRuns option').not(':disabled');
        var OptionNotDisabledDB = $('select.runDetailsManageRuns option').not(':disabled');
        $(OptionNotDisabled).remove();
        $(OptionNotDisabledDB).remove();
        $('select.databaseManageRuns').val('default');
        $('select.runDetailsManageRuns').val('default');
        $('#runTableWrap').remove();
        $('#hideForNew').show();
        $('#hideForNew').removeClass('dontValidate');
        $('#runDetailsAdded').remove();
        getClientDatabaseManage(clientID);

    });
    $('select.databaseManageRuns').change(function () {
        var databaseID = $(this).val();
        var OptionNotDisabledDB = $('select.runDetailsManageRuns option').not(':disabled');
        $(OptionNotDisabledDB).remove();
        $('select.runDetailsManageRuns option').val('default');
        getClientRunDetailsManage(databaseID);
        $('#runTableWrap').remove();
        $('#hideForNew').show();
        $('#hideForNew').removeClass('dontValidate');
        $('#runDetailsAdded').remove();

    });
    $('select.runDetailsManageRuns').change(function () {
        var runID = $(this).val();
        $('#runTableWrap').remove();
        runDetailsTable(runID)

    });
    //////////////////////////////////////////////
    /////// ADDITIONAL RUN PAGE //////////////////
    //////////////////////////////////////////////
    $('.clientAddRuns').change(function () {
        var clientID = $(this).val();
        var OptionNotDisabled = $('select.databaseAddRuns option').not(':disabled');
        $(OptionNotDisabled).remove();
        var OptionTableNotDisabled = $('select.table_nameNew option').not(':disabled');
        $(OptionTableNotDisabled).remove();
        $('select.databaseAddRuns').val('default');
        getClientDatabaseAdd(clientID);

    });
    $('select.databaseAddRuns').change(function () {
        var databaseID = $(this).val();
        var OptionTableNotDisabled = $('select.table_nameNew option').not(':disabled');
        $(OptionTableNotDisabled).remove();
        $('select.table_nameNew option').val('default');
        getAddTables(databaseID);

        function getAddTables(dbid) {
            var tableURL = "http://172.21.132.79/api/database/" + dbid + "/table";
            $.get(tableURL, function (data) {

                for (i = 0; i < data.length; i++) {

                    $('select.table_nameNew').append($('<option>').text(data[i].TableName).attr('value', data[i].TableName));
                    $('#table_schema').val(data[i].TableName)


                }

            });
        }
    });

    //////////////////////////////////////////////
    ///////// MANAGING RUN PAGE FUNCTIONS ////////
    //////////////////////////////////////////////
    function getClientDatabaseManage(client) {

        var databaseURL = "http://172.21.132.79/api/client/" + client + "/database/";
        $.get(databaseURL, function (data) {
            for (i = 0; i < data.length; i++) {
                var checkArrayAssocRun = [];


                for (i = 0; i < data.length; i++) {

                    if (jQuery.inArray(data[i].DBinfoID, checkArrayAssocRun) !== -1) {

                    } else {
                        $('select.databaseManageRuns').append($('<option>').text(data[i].Project).attr('value', data[i].DBinfoID));
                        checkArrayAssocRun.push(data[i].DBinfoID);

                    }
                }
                checkArrayAssocRun = [];


            }

        });
    }
    function getClientDatabaseAdd(client) {

        var databaseURL = "http://172.21.132.79/api/client/" + client + "/database/";
        $.get(databaseURL, function (data) {
            for (i = 0; i < data.length; i++) {
                var checkArrayAssocRun = [];


                for (i = 0; i < data.length; i++) {

                    if (jQuery.inArray(data[i].DBinfoID, checkArrayAssocRun) !== -1) {

                    } else {
                        $('select.databaseAddRuns').append($('<option>').text(data[i].Project + ' (' + data[i].DBinfoID + ') ').attr('value', data[i].DBinfoID));
                        checkArrayAssocRun.push(data[i].DBinfoID);

                    }
                }
                checkArrayAssocRun = [];


            }

        }).done(function () {
            $('#addNewRunDetailSubmit').click(function () {
                var table_nameNew = $('select.table_nameNew').val();
                var databaseIDAdd = $('.databaseAddRuns').val();
                var tableURL = "http://172.21.132.79/api/database/" + databaseIDAdd + "/table";
                $.get(tableURL, function (data) {
                    for (i = 0; i < data.length; i++) {
                        if (data[i].TableName == table_nameNew) {
                            $('#table_schemaNew').val(data[i].TableSchema);
                        }
                    }
                });
                //                                                                                       
                var databaseAddRuns = $('.databaseAddRuns').val();
                var addRunDetailsNew = $('#addRunDetailsNew').val();
                var aggregationNew = $('.aggregationNew').val();
                var table_nameNew = $('.table_nameNew').val();
                var table_schemaNew = $('#table_schemaNew').val();
                var filterNew = $('#filterNew').val();

                $.ajax({
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    url: 'http://172.21.132.79/api/rundetail',
                    type: 'POST',
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify({
                        DBinfoID: databaseAddRuns,
                        display_name: addRunDetailsNew,
                        aggregation: aggregationNew,
                        table_name: table_nameNew,
                        table_schema: table_schemaNew,
                        filter: filterNew
                    }),
                    error: function (e) {
                        console.log(e);
                    },
                    beforeSend: function () {
                        $('.relDiv, #cloud').show();
                        $('#loader').fadeIn(100);
                    },
                    complete: function () {
                        $('#contFive').find('[type="text"]').val(null);
                        $('.groupClientID').val('default');

                    },
                    success: function (result) {

                        $('.relDiv, #cloud').delay(2000).fadeOut(300);
                        setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                        setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                        $('#loader').delay(4000).fadeOut();
                        console.log(result);
                    }
                })
                .done(function () {
                    var addRemove = $('#addSection').find(':text');
                    var addRemoveSelect = $('#addSection').find(':text');

                    var OptionNotDisabled = $('select.databaseAddRuns option').not(':disabled');
                    $(OptionNotDisabled).remove();

                    var OptionTableNotDisabled = $('select.table_nameNew option').not(':disabled');
                    $(OptionTableNotDisabled).remove();

                    var OptionclientNotDisabled = $('select.clientAddRuns option').not(':disabled');
                    $(OptionclientNotDisabled).remove();

                    $('.databaseAddRuns').val('default');
                    $('.clientAddRuns').val('default');
                    $('.aggregationNew').val('default');
                    $('.table_nameNew').val('default');

                    addRemove.val('');
                    addRemoveSelect.val('');




                });

            });

        });
    }



    function getClientRunDetailsManage(database) {
        var runDetailURL = "http://172.21.132.79/api/database/" + database + "/RunDetail"

        $.get(runDetailURL, function (data) {
            for (i = 0; i < data.length; i++) {
                var checkArrayAssocRuns = [];


                for (i = 0; i < data.length; i++) {

                    if (jQuery.inArray(data[i].runDetailID, checkArrayAssocRuns) !== -1) {

                    } else {
                        $('select.runDetailsManageRuns').append($('<option>').text(data[i].display_name).attr('value', data[i].runDetailID));
                        checkArrayAssocRuns.push(data[i].runDetailID);

                    }
                }
                checkArrayAssocRuns = [];



            }

        })
        .done(function (data) {
            if (data.length < 1) {

                $('.cont').fadeIn(500);
                $('.cont').attr('data', 'active');
            }

        });

    }
    function runDetailsTable(runID) {

        var runURL = 'http://172.21.132.79/api/rundetail/' + runID;

        $.get(runURL, function (data) {

            $('.runDetailsManageRuns').after('<div id="runTableWrap"><div class="tableWrapS"><table class="runTable"><tr class=\"headerRow\"><td id=\"\" >runDetailID</td><td >DBinfoID</td><td >table_name</td><td >field_name</td><td >aggregation</td><td>filter</td><td>grouping</td><td>isActive</td><td>table_schema</td><td>type_flag</td><td>frequency_flag</td><td>display_name</td></tr><tr><td id=\"tableVal\" class=\"inputClientID rundetails_runDetailID\" >' + data.runDetailID + '</td><td class=\"\" id=\"rundetails_DBinfoID\">' + data.DBinfoID + '</td><td class=\"\" id=\"rundetails_table_name\">' + data.table_name + '</td><td class=\"inputABLE\" id=\"rundetails_field_name\" >' + data.field_name + '</td><td class=\"inputABLE\" id=\"rundetails_aggregation\" >' + data.aggregation + '</td><td class=\"inputABLE\" id=\"rundetails_filter\" >' + data.filter + '</td><td class=\"inputABLE\" id=\"rundetails_grouping\" >' + data.grouping + '</td><td class=\"inputABLE\" id=\"rundetails_isActive\" >' + data.isActive + '</td><td class=\"inputABLE\" id=\"rundetails_table_schema\" >' + data.table_schema + '</td><td class=\"inputABLE\" id=\"rundetails_type_flag\" >' + data.type_flag + '</td><td class=\"inputABLE\" id=\"rundetails_frequency_flag\" >' + data.frequency_flag + '</td><td class=\"inputABLE\" id=\"rundetails_display_name\" >' + data.display_name + '</td></tr> </table></div><table width="100%"><tr><td><div class=\"editCont\"><div id=\"updated\"></div><div id=\"refreshed\"></div><div class=\"editCont\"><div class=\"editRunOptionUpdate\"><i class=\"fa fa-check\"></i></div><div class=\"editRunOptionReload\"><i class=\"fa fa-refresh\"></i></div</div></td></tr></table></div>');

            $(".runTable td.inputABLE").dblclick(function () {
                newInput(this);
            });

            function closeInput(elm) {
                var value = $(elm).find('input').val();
                $(elm).empty().text(value);

                $(elm).bind("dblclick", function () {
                    newInput(elm);
                });
            }

            function newInput(elm) {
                $(elm).unbind('dblclick');

                var value = $(elm).text();
                $(elm).empty();

                $("<input>")
					.attr('type', 'text')
					.attr('class', 'inputABLE')
					.val(value)
					.blur(function () {
					    closeInput(elm);
					})
					.appendTo($(elm))
					.focus();
            }

            $('.editRunOptionUpdate').click(function () {

                $('.runTable td.inputABLE').each(function () {
                    if ($(this).text() == 'null') {
                        $(this).empty();
                    }

                });
                var one = $('.rundetails_runDetailID').text();
                var two = $('#rundetails_DBinfoID').text();
                var three = $('#rundetails_table_name').text();
                var four = $('#rundetails_field_name').text();
                var five = $('#rundetails_aggregation').text();
                var six = $('#rundetails_filter').text();
                var seven = $('#rundetails_grouping').text();
                var eight = $('#rundetails_isActive').text();
                var nine = $('#rundetails_table_schema').text();
                var ten = $('#rundetails_type_flag').text();
                var eleven = $('#rundetails_frequency_flag').text();
                var twelve = $('#rundetails_display_name').text();
                var updateButton = $(this).parent().parent().find('#updated');
                updateRunValues(one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, updateButton);
                $('.runTable td.inputABLE').each(function () {
                    if ($(this).text() == "") {
                        $(this).text('null');
                    }

                });

            });
            $('.editRunOptionReload').click(function () {
                var refreshButton = $(this).parent().parent().find('#refreshed');
                var one = $('.rundetails_runDetailID').text();
                refreshRunValues(one, refreshButton);
            });

        });
    }

    function updateRunValues(runDetailID, DBinfoID, table_name, field_name, aggregation, filter, grouping, isActive, table_schema, type_flag, frequency_flag, display_name, thisOne) {

        var databaseUrl = 'http://172.21.132.79/api/rundetail/' + runDetailID;

        $.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: databaseUrl,
            type: 'PUT',
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                runDetailID: runDetailID,
                DBinfoID: DBinfoID,
                table_name: table_name,
                field_name: field_name,
                aggregation: aggregation,
                filter: filter,
                grouping: grouping,
                isActive: isActive,
                table_schema: table_schema,
                type_flag: type_flag,
                frequency_flag: frequency_flag,
                display_name: display_name
            }),
            error: function (e) {
                console.log(e);
                $(thisOne)
					.text('Could Not Update')
					.hide()
					.css({
					    'color': 'white',
					    'background-color': 'red'

					})
                    .attr('display', 'block')
					.outerWidth('150');

                $(thisOne).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
            },
            beforeSend: function () {
                $('#loader').fadeIn(100);
            },
            complete: function () {
                $('#contThree').find('[type="text"]').val(null);
                $('#loader').delay(3000).fadeOut();
            },
            success: function (result) {
                $(thisOne)
					.text('Updated')
					.hide()
					.attr('display', 'block')
					.css({
					    'color': 'white',
					});
                $(thisOne).delay(3000).fadeIn(1000).delay(3000).fadeOut(1000);

            }
        });

    }



    function refreshRunValues(runID, thisOne) {

        var thisIsIt = thisOne;
        var databaseUrl = 'http://172.21.132.79/api/rundetail/' + runID;
        $.get(databaseUrl, function (data) {

            var one = $('.rundetails_runDetailID');
            var two = $('#rundetails_DBinfoID');
            var three = $('#rundetails_table_name');
            var four = $('#rundetails_field_name');
            var five = $('#rundetails_aggregation');
            var six = $('#rundetails_filter');
            var seven = $('#rundetails_grouping');
            var eight = $('#rundetails_isActive');
            var nine = $('#rundetails_table_schema');
            var ten = $('#rundetails_type_flag');
            var eleven = $('#rundetails_frequency_flag');
            var twelve = $('#rundetails_display_name');

            one.text(data.runDetailID);
            two.text(data.DBinfoID);
            three.text(data.table_name);
            four.text(data.field_name);
            five.text(data.aggregation);
            six.text(data.filter);
            seven.text(data.grouping);
            eight.text(data.isActive);
            nine.text(data.table_schema);
            ten.text(data.type_flag);
            eleven.text(data.frequency_flag);
            twelve.text(data.display_name);

        })
			.done(function () {
			    $(thisIsIt)
					.hide()
					.text('Refreshed')
					.attr('display', 'block')
					.css('color', 'white');
			    $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

			})
			.fail(function () {
			    $(thisIsIt)
					.hide()
					.text('Could Not Refresh')
					.attr('display', 'block')
					.css({
					    'color': 'white',
					    'background-color': 'red'

					})
					.outerWidth('150');
			    $(thisIsIt).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
			});
    }
    //////////////////////////////////////////////
    /////// ADDITIONAL RUN PAGE //////////////////
    //////////////////////////////////////////////




    function getClients(init, after, where, whereSecond) {
        $.get("http://172.21.132.79/api/client", function (data) {
            var clientList = data;
            for (i = 0; i < clientList.length; i++) {
                var serverURL = "http://172.21.132.79/api/client/" + data[i].clientID + "/server/";
                if (init == true) {
                    $('select.client,select.clientID,select.groupClientID,select.clientClass,select.clientManageRuns,select.clientAddRuns,.clientScope,.clientScopeApp').append($('<option>').text(data[i].client_display_name).attr('value', data[i].clientID));
                }
                if (after == true) {
                    $(where).append($('<option>').text(data[i].client_display_name).attr('value', data[i].clientID));
                }

            }
        }).done(function () {

            if (after == true) {
                where.change(function () {
                    whereSecond.empty();
                    getServers(whereSecond);
                });
            }
        });
    }
    function getServers(where) {
        var whereID = where.parent().find('select:first').val();
        var url = "http://172.21.132.79/api/client/" + whereID + "/server/";
        $.get(url, function (data) {

            var list = [];

            for (i = 0; i < data.length; i++) {

                if (jQuery.inArray(data[i].serverInfoID, list) !== -1) {
                } else {
                    where.append($('<option>').text(data[i].Linked_Server).attr('value', data[i].serverInfoID));

                    list.push(data[i].serverInfoID);
                }

            }


        }).done(function () {
            list = [];
        });
    }
    getAll($('.searchDiv'), false);
    $('.refreshList').click(function () {
        var thisSearch = $(this).parent().parent().find('.searchDiv');

        thisSearch.fadeOut(300);
        setTimeout(function () {
            thisSearch.empty();
        }, 2000);
        setTimeout(function () {
            getAll(thisSearch, true);

        }, 3000);



    });
    function getAll(_this, triggeredRefresh) {
        var amount = 0;
        $(_this).each(function () {

            var id = $(this).attr('id');
            var thisCont = $(this);
            var URL = "http://172.21.132.79/api/" + id;

            $.get(URL, function (data) {
                $.each(data, function (index, obj) {
                    var objLen = Object.keys(obj).length;
                    var keys = Object.keys(obj);
                    var keyID = obj[keys[0]];

                    $('#' + id).append('<div class=\"bridge\"><div class=\"abTick\"><div class=\"centTick\"><div class=\"trigger\"></div><svg version=\"1.1\" style=\"width:40px;\" id=\"tick\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 37 37\" style=\"enable-background:new 0 0 37 37;\" xml:space=\"preserve\"><path class=\"circ path\" style=\"fill:none;stroke:green;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;\" d=\"M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z\"/><polyline class=\"tick path\" style=\"fill:none;stroke:green;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;\" points=\"11.6,20 15.9,24.2 26.4,13.8 \"/></svg></div></div><div class=\"cellRow \" id=\'' + id + keyID + '\'><div class=\"absoluteButton\"><i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i></div></div><div class=\"editCont1\"><div id=\"updated\"></div><div id=\"refreshed\"></div><div id=\"deleted\"></div><div class=\"editCont\"><div class=\"editOptionUpdate2\"><i class=\"fa fa-check\"></i></div><div class=\"editOptionReload2\"><i class=\"fa fa-refresh\"></i></div><div class=\"editOptionDelete2\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div></div></div></div>');
                    for (var i = 0; i < objLen; i++) {

                        if (obj[keys[i]] === null || obj[keys[i]] == 'null' || obj[keys[i]] === '') {
                            $('#' + id + keyID).append('<div class=\"divNormalCell editAble\" id=\"' + keys[i] + '\"></div>');
                        } else {
                            $('#' + id + keyID).append('<div class=\"divNormalCell editAble\" id=\"' + keys[i] + '\">' + obj[keys[i]] + '</div>');
                        }

                    }
                });

            }).done(function () {
                amount++;
                var thisButEdit = _this.find('.editAble');
                $.each($('.divNormalCell'), function () {
                    var regex = new RegExp('.*.ID');
                    var divID = $(this).attr('id');
                    if (regex.test(divID)) {
                        console.log(this);
                        $(this).removeClass('editAble');
                    }
                });
                if (amount === 5 || triggeredRefresh == true) {

                    if (triggeredRefresh == true) {
                        $(thisButEdit).append('<div class=\"wrappedEdit\"><div class=\'balancer\'><div class="editButtonsFinal">');
                        $('.wrappedEdit').hide();
                        $(thisButEdit).each(function () {
                            hoverEdit(this);
                        });
                        secondButton(_this);
                        console.log('lool');
                        _this.fadeIn(300);

                    } else {

                        buttonsClick();

                        $(thisButEdit).append('<div class=\"wrappedEdit\"><div class=\'balancer\'><div class="editButtonsFinal">');

                        $('.wrappedEdit').hide();

                        $(thisButEdit).each(function () {
                            hoverEdit(this);

                        });
                        $('.editAble').dblclick(function () {
                            editAble(this);

                        });
                    }


                    $('.editCont1').hide();
                    $('.cellRow:odd').css('background-color', '#ccc')
                    $('.absoluteButton').dblclick(function (e) {
                        e.preventDefault();
                    });
                    $('.absoluteButton').hover(function (e) {
                        e.preventDefault();
                    });

                    $('.divTableHeaders').each(function () {
                        var totalChildren = $(this).parent().children().not('.refreshList,.addType').length;
                        var divisionPercentage = (100 / totalChildren) + '%';
                        $(this).outerWidth(divisionPercentage);
                        var normCell = $(this).parent().parent().find('.divNormalCell');
                        normCell.outerWidth(divisionPercentage);
                        normCell.addClass('divisionPercentage');
                    });

                    $('.editOptionUpdate2').each(function () {
                        $(this).click(function () {
                            var trigger = $(this).parent().parent().parent().find('.trigger');
                            var urlID = $(this).parent().parent().parent().parent().attr('id');
                            var updated = $(this).parent().parent().find('#updated');
                            var Wrapps = $(this).parent().parent().parent();
                            var allVals = Wrapps.find('.divNormalCell');
                            var allValLength = allVals.length;

                            var vals = {};
                            $(allVals).each(function () {
                                var id = $(this).attr('id');
                                var texts = $(this).text();

                                vals[id] = texts;

                            });
                            var idK = [];
                            var iK = [];

                            for (var k in vals) {
                                if (vals.hasOwnProperty(k)) {
                                    idK.push(vals[k]);
                                    iK.push(k);
                                }
                            }


                            var thisURL = "http://172.21.132.79/api/" + urlID.toLowerCase() + '/' + idK[0];

                            var stringedData = JSON.stringify(vals);
                            $.ajax({
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                url: thisURL,
                                type: 'PUT',
                                dataType: "json",
                                contentType: "application/json",
                                data: stringedData,
                                error: function (e) {
                                    console.log(e);
                                    $(updated)
										.text('Could Not Update')
										.hide()
										.attr('display', 'block')
										.outerWidth('150')
										.attr('color', 'red');
                                    $(updated).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
                                },
                                beforeSend: function () {
                                    $('.relDiv, #cloud').show();
                                    $('#loader').fadeIn(100);

                                },
                                complete: function () {
                                    $('#contThree').find('[type="text"]').val(null);
                                    $('#loader').delay(3000).fadeOut();
                                },
                                success: function (result) {
                                    $('.relDiv, #cloud').delay(2000).fadeOut(300);
                                    setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                                    setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                                    $('#loader').delay(4000).fadeOut();
                                    $(updated)
									.hide()
									.text('Refreshed')
									.attr('display', 'block')
									.css('background-color', 'green');
                                    $(updated).delay(4000).fadeIn(1000).delay(1000).fadeOut(1000);
                                }
                            });
                        });
                    });
                    $('.editOptionReload2').each(function () {
                        $(this).click(function () {

                            var trigger = $(this).parent().parent().parent().find('.trigger');
                            var urlID = $(this).parent().parent().parent().parent().attr('id');
                            var refreshed = $(this).parent().parent().find('#refreshed');
                            var Wrapps = $(this).parent().parent().parent();
                            var allVals = Wrapps.find('.divNormalCell');
                            var allValLength = allVals.length;

                            var vals = {};
                            $(allVals).each(function () {
                                var id = $(this).attr('id');
                                var texts = $(this).text();

                                vals[id] = texts;

                            });
                            var idK = [];
                            var iK = [];



                            for (var k in vals) {
                                if (vals.hasOwnProperty(k)) {
                                    idK.push(vals[k]);
                                    iK.push(k);
                                }
                            }

                            var thisURL = "http://172.21.132.79/api/" + urlID.toLowerCase() + '/' + idK[0];

                            $.get(thisURL, function (data) {

                                for (i = 0; i < allValLength; i++) {
                                    var datakeys = iK[i];
                                    if (data[datakeys] == null || data[datakeys] == 'null' || data[datakeys] == '') {
                                        allVals.parent().find('#' + datakeys).text('');
                                    } else {

                                        allVals.parent().find('#' + datakeys).text(data[datakeys]);
                                    }

                                }

                            })
							.done(function () {
							    $(refreshed)
									.hide()
									.text('Refreshed')
									.attr('display', 'block')
									.css('background-color', 'green');
							    $(refreshed).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);
							    $('.divTableHeaders').each(function () {
							        var totalChildren = $(this).parent().children().not('.refreshList,.addType').length;
							        var divisionPercentage = (100 / totalChildren) + '%';
							        $(this).outerWidth(divisionPercentage);
							        var normCell = $(this).parent().parent().find('.divNormalCell');
							        normCell.outerWidth(divisionPercentage);
							    });

							})
							.fail(function () {
							    $(refreshed)
									.hide()
									.text('Could Not Refresh')
									.attr('display', 'block')
									.css('background-color', 'red')
									.outerWidth('150');
							    $(refreshed).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);


							});
                        });
                    });
                    $('.editOptionDelete2').each(function () {
                        $(this).click(function () {

                            var trigger = $(this).parent().parent().parent().find('.trigger');
                            var urlID = $(this).parent().parent().parent().parent().attr('id');
                            var deleted = $(this).parent().parent().find('#deleted');
                            var Wrapps = $(this).parent().parent().parent();
                            var allVals = Wrapps.find('.divNormalCell');
                            var allValLength = allVals.length;

                            var vals = {};
                            $(allVals).each(function () {
                                var id = $(this).attr('id');
                                var texts = $(this).text();

                                vals[id] = texts;

                            });
                            var idK = [];
                            var iK = [];

                            for (var k in vals) {
                                if (vals.hasOwnProperty(k)) {
                                    idK.push(vals[k]);
                                    iK.push(k);
                                }
                            }

                            var thisURL = "http://172.21.132.79/api/" + urlID.toLowerCase() + '/' + idK[0];

                            $.ajax({
                                url: thisURL,
                                type: 'DELETE',
                            })
							.done(function () {
							    $(deleted)
									.hide()
									.text('Deleted')
									.attr('display', 'block')
									.css('background-color', 'green');
							    $(Wrapps).slideUp('slow');

							    setTimeout(function () {
							        $(Wrapps).remove();
							    }, 1000);


							})
							.fail(function () {
							    $(refreshed)
									.hide()
									.text('Could Not Refresh')
									.attr('display', 'block')
									.css('background-color', 'red')
									.outerWidth('150');
							    $(refreshed).delay(0).fadeIn(1000).delay(3000).fadeOut(1000);

							});
                        });
                    });
                    amount = 0;
                }

            });
        });
    }




    $(".search").each(function () {
        $(this).keyup(function () {
            _this = this;
            _thisBridge = $(this).parent().find('.bridge');
            $.each(_thisBridge, function () {
                if ($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                    $(this).slideUp('fast');
                else
                    $(this).slideDown('fast');
            });
        });
    });
    $(".clientScopeApp").change(function () {
        var where = $('.ScopeApp');
        var whereNotDefault = $('.ScopeApp option').not(':disabled');
        $(whereNotDefault).remove();
        $('.ScopeApp').val('default');
        getScopes(where);
    });

});

function getScopes(where) {

    var whereID = where.parent().parent().find('select:first').val();
    console.log(where);
    var url = "http://172.21.132.79/api/client/" + whereID + "/scope/";
    $.get(url, function (data) {

        var list = [];

        for (i = 0; i < data.length; i++) {

            if (jQuery.inArray(data[i].scopeID, list) !== -1) {
            } else {
                where.append($('<option>').text(data[i].scope_name).attr('value', data[i].scopeID));

                list.push(data[i].scopeID);
            }

        }


    }).done(function () {
        $('#addNewRunScope').click(function () {

            var clientScopeApp = $('.clientScopeApp').val();
            var ScopeApp = $('.ScopeApp').val();
            var run_name = $('#run_name').val();
            var run_order = $('#run_order').val();
            var runInfoID = $('#runInfoID').val();
            console.log(clientScopeApp, ScopeApp, run_name, run_order);

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                url: 'http://172.21.132.79/api/runscope',
                type: 'POST',
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    run_name: run_name,
                    clientID: clientScopeApp,
                    scopeID: ScopeApp,
                    run_order: run_order,
                    runInfoID: runInfoID
                }),
                error: function (e) {
                    console.log(e);
                    $('#contNine [type=text]').val();
                    var whereNotDefault = $('.ScopeApp option').not(':disabled');
                    $(whereNotDefault).remove();
                    $('.ScopeApp, .clientScopeApp').val('default');
                    $('.relDiv, #cloud').delay(2000).fadeOut(300);
                    $('#loader').delay(2000).fadeOut();
                    alert('disshitnoworkbro');
                    console.log(result);
                },
                beforeSend: function () {
                    $('.relDiv, #cloud').show();
                    $('#loader').fadeIn(100);
                },
                complete: function () {
                    $('#contFive').find('[type="text"]').val(null);
                    $('.groupClientID').val('default');


                },
                success: function (result) {
                    $('#contNine [type=text]').val();
                    var whereNotDefault = $('.ScopeApp option').not(':disabled');
                    $(whereNotDefault).remove();
                    $('.ScopeApp, .clientScopeApp').val('default');
                    $('.relDiv, #cloud').delay(2000).fadeOut(300);
                    setTimeout(function () { $('#fixer .trigger').addClass('drawn'); }, 2300);
                    setTimeout(function () { $('#fixer .trigger').removeClass('drawn'); }, 3300);
                    $('#loader').delay(4000).fadeOut();
                    console.log(result);
                }
            });
        });
    });
}

//////////////////////////////////////////////
////////////// LOST N' FOUND /////////////////
//////////////////////////////////////////////
function buttonsClick() {
    $('.absoluteButton').each(function () {
        $(this).parent().next('.editCont1').slideUp();
        $(this).css('z-index', '1');
        $(this).click(function () {
            $(this).toggleClass('activeButtons');
            $(this).find('i').toggleClass('activeButtons1');
            if ($(this).hasClass('activeButtons')) {
                $(this).parent().next('.editCont1').slideDown().css('display', 'block');
            } else {
                $(this).parent().next('.editCont1').slideUp();
            }
        });
    });

}
function secondButton(_this) {
    var thisFind = $(_this).find('.absoluteButton');
    var thisEditAble = $(_this).find('.editAble');
    var i = 0;
    $(thisFind).each(function () {
        i++
        $(this).parent().next('.editCont1').slideUp();
        $(this).css('z-index', '1');
        $(this).click(function () {
            $(this).toggleClass('activeButtons');
            $(this).find('i').toggleClass('activeButtons1');
            if ($(this).hasClass('activeButtons')) {
                $(this).parent().next('.editCont1').slideDown().css('display', 'block');
            } else {
                $(this).parent().next('.editCont1').slideUp();
            }
        });
    });
    $(thisEditAble).dblclick(function () {
        editAble(this);

    });


}
function editAble(elm) {
    $(elm).unbind('dblclick');
    var innerText = $(elm).text();
    $(elm).contents().filter(function () {
        return this.nodeType === 3;
    }).remove();
    $(elm).find('.wrappedEdit').addClass('noWrap');
    $(elm).find('.wrappedEdit').hide();
    $(elm).addClass('paddingClass');
    $("<input>")
		.attr('type', 'text')
		.attr('class', 'inputABLE')
		.css('background-color', 'white')
		.val(innerText)
		.blur(function () {
		    editAbleClose(elm);
		})
		.appendTo($(elm))
		.focus();

}

function editAbleClose(elm) {
    var value = $(elm).find('input').val();
    $(elm).find('input').remove();
    $(elm).find('.wrappedEdit').removeClass('noWrap');
    $(elm).prepend(value);
    $(elm).removeClass('paddingClass');
    $(elm).bind("dblclick", function () {
        editAble(elm);
    });

}
function hoverEdit(elm, lol) {
    $(elm).hover(function () {
        var wrappss = $(this).find('.wrappedEdit');
        if (wrappss.hasClass('noWrap')) {

        } else {
            $(wrappss).toggle();
        }

    });
}
