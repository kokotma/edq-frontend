$(document).ready(function () {

    //		document.oncontextmenu = document.body.oncontextmenu = function() {return false;}

    //	$('#loader').removeCLass('hide');
    $('nightButton').click(function () {

    });

    var rows = $('main').filter(function () {
        var color = $(this).css("background-color");
        return color == "#ffffff" || color ==
			"#F5F5F5";
    });
    rows.addClass('no');
    $('.menuIcon').click(function () {
        $('.setWidthNav').toggleClass('showNav');

    });
    $("#sideNav").click(function (event) {
        event.stopPropagation();
    });
    $(this).click(function () {
        $('#mainCont').click(function () {
            $('.setWidthNav').removeClass('showNav');
            $('#burger').removeClass('active-sandwich');

        });
    });

    $('.menuIcon').click(function () {
        $('#burger').toggleClass('active-sandwich');
    });
    function animatethis(targetElement, speed) {
        $(targetElement).animate({
            marginLeft: "0px",
            marginTop: "0px"
        }, {
            duration: speed,
            complete: function () {
                targetElement.animate({
                    marginLeft: "-30px",
                    marginTop: "-50px"
                }, {
                    duration: speed,
                    complete: function () {
                        animatethis(targetElement, speed);
                    }
                });
            }
        });

    };


    animatethis($('#sunImg'), 1500);

    $('#loader')
        .removeClass('hide')
        .hide();

    $('.toggleChecked').click(function () {
        $('#tableYes').click();
    });

    $('#runDetailShow,#ScopeShow').slideUp('fast');

    $('#RunDetailsItem').click(function () {
        $('#RunDetailsItem .arrow').toggleClass('activeArrow');
        $('#runDetailShow').slideToggle();
    });
    $('#ScopeItem').click(function () {
        $('#ScopeItem .arrow').toggleClass('activeArrow');
        $('#ScopeShow').slideToggle();
    });

    $('#contTwo, #contThree, #contFour, #contFive, #contSix, #contSeven, #contNine').removeAttr('display')
    $('.itemLine').removeClass('changed');
    $('#itemOne').next('.itemLine').addClass('changed');
    $('#itemOne').find('.navText').addClass('greenText');
    var numbers = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    for (i = 0; i < numbers.length; i++) {
        $('#cont' + numbers[i]).hide();
    }
    $('#contOne').show();

    $('#item-One').next('.itemLine').addClass('changed');
    $('#item-One').find('.navText').addClass('greenText');
    $('.itemNav').each(function () {
        $(this).click(function () {
            $('.itemLine').removeClass('changed');
            $('.navText').removeClass('greenText');
            $(this).next('.itemLine').addClass('changed');
            $(this).find('.navText').addClass('greenText');
            var toSplit = $(this).attr('id');
            var splitID = toSplit.split('-');
            for (i = 0; i < numbers.length; i++) {
                $('#cont' + numbers[i]).hide();
            }
            $('#cont' + splitID[1]).show();

        });
    });
});

$(document).ready(function () {
    $('select').change(function () {
        var value = $(this).val();
        $(this).siblings('select').children('option').each(function () {
            if ($(this).val() === value) {
                $(this).attr('disabled', true).siblings().removeAttr('disabled');
            }
        });

    });
    $('.client').change(function () {
        var value = $(this).val();
    });
    $('#addSection').hide();
    $('#manageRunChanger').click(function () {
        $('#addGroupChanger').removeClass('activeChanger');
        $(this).addClass('activeChanger');
        $('#addSection').hide();
        $('#manageSection').fadeIn(500);

    });
    $('#addGroupChanger').click(function () {
        $('#manageRunChanger').removeClass('activeChanger');
        $(this).addClass('activeChanger');
        $('#manageSection').hide();
        $('#addSection').fadeIn(500);

    });
    $('#addScopeSection').hide();
    $('#manageRunScopeChanger').click(function () {
        $('#addRunScopeChanger').removeClass('activeChanger');
        $(this).addClass('activeChanger');
        $('#addScopeSection').hide();
        $('#manageScopeSection').fadeIn(500);

    });
    $('#addRunScopeChanger').click(function () {
        $('#manageRunScopeChanger').removeClass('activeChanger');
        $(this).addClass('activeChanger');
        $('#manageScopeSection').hide();
        $('#addScopeSection').fadeIn(500);

    });

    $('#addGroupSection').hide();
    $('#manageGroupChanger').click(function () {
        $('#addGroupChanger2').removeClass('activeChanger');
        $(this).addClass('activeChanger');
        $('#addGroupSection').hide();
        $('#manageGroupSection').fadeIn(500);

    });
    $('#addGroupChanger2').click(function () {
        $('#manageGroupChanger').removeClass('activeChanger');
        $(this).addClass('activeChanger');
        $('#manageGroupSection').hide();
        $('#addGroupSection').fadeIn(500);

    });

    $('#CurrentPage').text($('.greenText .hideText').text());
    $('.itemNav').click(function () {
        $('#CurrentPage').text($('.greenText .hideText').text());
    });

    var nums = ['1', '2', '3', '4'];
    for (i = 0; i < nums.length; i++) {
        $('#hideShow' + nums[i]).hide();
    }
    $('#hideShow1').show();
    $('.allSections').each(function () {
        $(this).click(function () {
            if (!$(this).hasClass('activeChanger')) {
                $('.allSections').removeClass('activeChanger');
                $(this).addClass('activeChanger');
                var toSplit = $(this).attr('id');
                var splitID = toSplit.split('-');
                for (i = 0; i < nums.length; i++) {
                    $('#hideShow' + nums[i]).hide();
                }
            }

            $('#hideShow' + splitID[1]).fadeIn(500);
        });
    });


});